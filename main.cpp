#include <stdio.h>
#include <stdlib.h>

#include <string>
#include <iostream>

#include "alf.h"
#include "includes/acc_matrix.h"
#include "lodepng.h"
#include "argvparser.h"
#include "common.h"

using namespace std;
using namespace CommandLineProcessing;

union shortAndByteUnion 
{
    unsigned short s;  // or use int16_t to be more specific
    //   vs.
    struct Byte 
    {
        unsigned char c1, c2;  // or use int8_t to be more specific
    }byte;
};

//profiler...
pstat_t g_pstats;

// Global variables
std::map<std::string, ull_t> g_profiling_stats;
std::string filename = "AT3_1m4_";
std::string filenames[10];
std::vector<unsigned char> image __attribute__ ((__aligned__(128)));
unsigned int image_width = 0;
unsigned int image_height = 0;
unsigned int num_blocks = 0; 
	
unsigned int num_pixels = 0;
unsigned int num_ON_pixels = 0;
unsigned int numAccel = 6;
unsigned int filenameCount = 1;
unsigned int outputBlockSize = 1;

unsigned int benchmarkIterations = 1;
bool hasOutput = true;

ArgvParser parser;

// ALF data structures
alf_handle_t alf_handle;
alf_wb_handle_t wb_handle;

int spuHysteresis(  float* in, float* out, 
                    unsigned int lowThreshold, unsigned int highThreshold, 
                    unsigned int blockSize)
{
    unsigned int standardBlockSize, finalBlockSize;
	div_t divresult;
	divresult = div(static_cast<int>(image_height - 8), static_cast<int>(blockSize));

	if(divresult.rem == 0){
		num_blocks = divresult.quot;
		standardBlockSize = blockSize;
		finalBlockSize = blockSize;
	}
	else{
		num_blocks = divresult.quot + 1;
		standardBlockSize = blockSize;
		finalBlockSize = divresult.rem;
	}

	printf("SPU Hystesis: StandardBlockSize/FinalBlockSize/NumBlocks = %d/%d/%d\n", 
            standardBlockSize, 
            finalBlockSize, 
            num_blocks ); 

    PROFILING_START("Hysteresis", "fill-perimeter")
    {
        unsigned int fourthLastRow = num_pixels - image_width * 4;
		for (unsigned int i=0; i < image_width*4; ++i)
        {
            out[i] = 0.0f;
			
        }
        for (unsigned int i=0; i < image_width*4; ++i)
        {
            out[fourthLastRow+i] = 0.0f;
        }
    }
    PROFILING_END()

	// Fill param_ctx struct with needed variables
	ctx_hysteresis_struct param_ctx __attribute__ ((aligned(128)));
    param_ctx.imageWidth = image_width;
	param_ctx.highThreshold = highThreshold;
	param_ctx.lowThreshold = lowThreshold;
	
    // Create task and task descriptor handles
    alf_task_handle_t task_handle;
    alf_task_desc_handle_t td_handle;

    // Names of SPU objects and functions
    char library_name[] = "spu_programs.so";   
    char spu_image_name[] = "spu_hysteresis";
    char kernel_name[] = "kernel";

    // *** SINGLE TASK STARTS HERE *** 

    PROFILING_START("Hysteresis", "init-and-create")
    {
        // Create and configure the task descriptor
        alf_task_desc_create(alf_handle, ALF_ACCEL_TYPE_SPE, &td_handle);

        // Identify the name of the object library
        alf_task_desc_set_int64(td_handle, ALF_TASK_DESC_ACCEL_LIBRARY_REF_L, (unsigned long long)library_name);

        // Identify the name of the SPU image
        alf_task_desc_set_int64(td_handle, ALF_TASK_DESC_ACCEL_IMAGE_REF_L, (unsigned long long)spu_image_name);

        // Identify the name of the kernel
        alf_task_desc_set_int64(td_handle, ALF_TASK_DESC_ACCEL_KERNEL_REF_L, (unsigned long long)kernel_name);

        // Configure the memory requirements and add the task context
        alf_task_desc_set_int32(td_handle, ALF_TASK_DESC_MAX_STACK_SIZE, 4096);

        // Configure memory for the input buffer (array_a)
        alf_task_desc_set_int32(td_handle, ALF_TASK_DESC_WB_IN_BUF_SIZE, image_width*(2+standardBlockSize)*sizeof(float));

        // Configure memory for the output buffer (array_b)
        alf_task_desc_set_int32(td_handle, ALF_TASK_DESC_WB_OUT_BUF_SIZE, image_width*standardBlockSize*sizeof(float));    

        // Configure memory for the work block parameter context
        alf_task_desc_set_int32(td_handle, ALF_TASK_DESC_WB_PARM_CTX_BUF_SIZE, sizeof(ctx_hysteresis_struct));

        // Create the task for cyclic work block distribution
        int ret;
        if ((ret = alf_task_create(td_handle, NULL, 0, 0, 0, &task_handle)) < 0) 
        {
            fprintf(stderr, "alf_task_create failed, ret=%d\n", ret);
            return 1;
        }
    }
    PROFILING_END()

    for (unsigned int i=0; i<num_blocks; i++) 
    {
        PROFILING_START("Hysteresis", "wrkblk-create-and-enqueue")
        {
            if (i != num_blocks-1)
    		{
    			param_ctx.numLines = standardBlockSize;
    		}
    		else
    		{
    			param_ctx.numLines = finalBlockSize;
    		}
    		
    		// Create the work block
            alf_wb_create(task_handle, ALF_WB_SINGLE, 0, &wb_handle);
            alf_wb_parm_add(wb_handle, (void *)(&param_ctx), sizeof(param_ctx), ALF_DATA_BYTE, 0);

            // DTL: Store intensity in an input buffer
            alf_wb_dtl_begin(wb_handle, ALF_BUF_IN, 0);
            alf_wb_dtl_entry_add(wb_handle, &in[image_width*3+i*image_width*standardBlockSize], 
                    image_width*(2+param_ctx.numLines), ALF_DATA_FLOAT);
            alf_wb_dtl_end(wb_handle);
    		
            // DTL: Tranfer the edge intensity into in the front half of the output array
            alf_wb_dtl_begin(wb_handle, ALF_BUF_OUT, 0);
            alf_wb_dtl_entry_add(wb_handle, &out[image_width*4+i*image_width*standardBlockSize], 
                    image_width*param_ctx.numLines, ALF_DATA_FLOAT);
            alf_wb_dtl_end(wb_handle);

            // Enqueue the work block in the task
            alf_wb_enqueue(wb_handle);
        }
        PROFILING_END()
    }

    // Finalise the task
    alf_task_finalize(task_handle);
    PROFILING_START("Hysteresis", "task-wait")
    {
        // Wait for task to complete
        alf_task_wait(task_handle, -1);
    }
    PROFILING_END()

	// Destroy task and task descriptor handles
	alf_task_desc_destroy(td_handle);
    alf_task_destroy(task_handle);

    // *** SINGLE TASK ENDS HERE *** 

    return 0;
}

int spuNonMaxSuppression(float* in, float* out, unsigned int blockSize)
{
    unsigned short standardBlockSize, finalBlockSize;
	div_t divresult;
	divresult = div(static_cast<int>(image_height - 8), static_cast<int>(blockSize));
	if(divresult.rem == 0)
	{
		num_blocks = divresult.quot;
		standardBlockSize = blockSize;
		finalBlockSize = blockSize;
	}
	else
	{
		num_blocks = divresult.quot + 1;
		standardBlockSize = blockSize;
		finalBlockSize = divresult.rem;
	}
	printf("SPU NMS: StandardBlockSize/FinalBlockSize/NumBlocks = %d/%d/%d\n", 
            standardBlockSize, finalBlockSize, num_blocks ); 
    PROFILING_START("NMS", "fill-perimeter")
    {
        const unsigned int fourthLastRow = num_pixels - image_width*4;
		for (unsigned int i=0; i < image_width*4; ++i)
        {
            out[i] = 0.0f;
			
        }
        for (unsigned int i=0; i < image_width *4; ++i)
        {
            out[fourthLastRow+i] = 0.0f;
        }

    }
    PROFILING_END()

	// Fill param_ctx struct with needed variables
	ctx_NMS_struct param_ctx __attribute__ ((aligned(128)));
    param_ctx.imageWidth = image_width;

    // Create task and task descriptor handles
    alf_task_handle_t task_handle;
    alf_task_desc_handle_t td_handle;

    // Names of SPU objects and functions
    char library_name[] = "spu_programs.so";   
    char spu_image_name[] = "spu_NMS";
    char kernel_name[] = "kernel";

    // *** SINGLE TASK STARTS HERE *** 
    PROFILING_START("NMS", "init-and-create")
    {
        // Create and configure the task descriptor
        alf_task_desc_create(alf_handle, ALF_ACCEL_TYPE_SPE, &td_handle);

        // Identify the name of the object library
        alf_task_desc_set_int64(td_handle, ALF_TASK_DESC_ACCEL_LIBRARY_REF_L, (unsigned long long)library_name);

        // Identify the name of the SPU image
        alf_task_desc_set_int64(td_handle, ALF_TASK_DESC_ACCEL_IMAGE_REF_L, (unsigned long long)spu_image_name);

        // Identify the name of the kernel
        alf_task_desc_set_int64(td_handle, ALF_TASK_DESC_ACCEL_KERNEL_REF_L, (unsigned long long)kernel_name);

        // Configure the memory requirements and add the task context
        alf_task_desc_set_int32(td_handle, ALF_TASK_DESC_MAX_STACK_SIZE, 4096);

        // Configure memory for the input buffer (array_a)
        alf_task_desc_set_int32(td_handle, ALF_TASK_DESC_WB_IN_BUF_SIZE, image_width*(2+standardBlockSize+standardBlockSize)*sizeof(float));

        // Configure memory for the output buffer (array_b)
        alf_task_desc_set_int32(td_handle, ALF_TASK_DESC_WB_OUT_BUF_SIZE, image_width*standardBlockSize*sizeof(float));    

        // Configure memory for the work block parameter context
        alf_task_desc_set_int32(td_handle, ALF_TASK_DESC_WB_PARM_CTX_BUF_SIZE, sizeof(ctx_NMS_struct));

        // Create the task for cyclic work block distribution
        int ret;
        if ((ret = alf_task_create(td_handle, NULL, 0, 0, 0, &task_handle)) < 0) 
        {
            fprintf(stderr, "alf_task_create failed, ret=%d\n", ret);
            return 1;
        }
    }
    PROFILING_END()

    for (unsigned int i=0; i<num_blocks; i++) 
    {
        PROFILING_START("NMS", "wrkblk-create-and-enqueue")
        {
            if (i != num_blocks-1)
        	{
        		param_ctx.numLines = standardBlockSize;
        	}
        	else
        	{
        		param_ctx.numLines = finalBlockSize;
        	}
        	
        	// Create the work block
            alf_wb_create(task_handle, ALF_WB_SINGLE, 0, &wb_handle);
            alf_wb_parm_add(wb_handle, (void *)(&param_ctx), sizeof(param_ctx), ALF_DATA_BYTE, 0);

            // DTL: Store intensity in an input buffer
            alf_wb_dtl_begin(wb_handle, ALF_BUF_IN, 0);
            alf_wb_dtl_entry_add(wb_handle, &in[image_width*3+i*image_width*standardBlockSize], 
                    image_width*(2+param_ctx.numLines), ALF_DATA_FLOAT);
            alf_wb_dtl_end(wb_handle);
        	
        	// DTL: Store directionality in an input buffer
            alf_wb_dtl_begin(wb_handle, ALF_BUF_IN, sizeof(float)*image_width*(2+param_ctx.numLines));
            alf_wb_dtl_entry_add(wb_handle, &in[image_width*4+i*image_width*standardBlockSize+num_pixels], 
                    image_width*param_ctx.numLines, ALF_DATA_FLOAT);
            alf_wb_dtl_end(wb_handle);

            // DTL: Tranfer the edge intensity into in the front half of the output array
            alf_wb_dtl_begin(wb_handle, ALF_BUF_OUT, 0);
            alf_wb_dtl_entry_add(wb_handle, &out[image_width*4+i*image_width*standardBlockSize], 
                    image_width*param_ctx.numLines, ALF_DATA_FLOAT);
            alf_wb_dtl_end(wb_handle);

            // Enqueue the work block in the task
            alf_wb_enqueue(wb_handle);
        }
        PROFILING_END()
    }

    // Finalise the task
    alf_task_finalize(task_handle);
    PROFILING_START("NMS", "task-wait")
    {
        // Wait for task to complete
        alf_task_wait(task_handle, -1);
    }
    PROFILING_END()

	// Destroy task and task descriptor handles
	alf_task_desc_destroy(td_handle);
    alf_task_destroy(task_handle);

    // *** SINGLE TASK ENDS HERE *** 

    return 0;
}

int spuSobel(float* in, float* out, unsigned int blockSize)
{
    unsigned short standardBlockSize, finalBlockSize;
	div_t divresult;
	divresult = div(static_cast<int>(image_height - 8), static_cast<int>(blockSize));
	
	if(divresult.rem == 0)
	{
		num_blocks = divresult.quot;
		standardBlockSize = blockSize;
		finalBlockSize = blockSize;
	}
	else
	{
		num_blocks = divresult.quot + 1;
		standardBlockSize = blockSize;
		finalBlockSize = divresult.rem;
	}
	printf("SPU SOBEL: StandardBlockSize/FinalBlockSize/NumBlocks = %d/%d/%d\n", 
            standardBlockSize, finalBlockSize, num_blocks ); 
    PROFILING_START("Sobel", "fill-perimeter")
    {
        unsigned int fourthLastRow = num_pixels - image_width*4;
		for (unsigned int i = 0; i < image_width*4; ++i)
        {
            out[i] = 0.0f;
			
        }
		for (unsigned int i = num_pixels; i < num_pixels+image_width*4; ++i)
        {
            out[i] = 0.0f;	
        }
        for (unsigned int i = 0; i < image_width*4; ++i)
        {
            out[fourthLastRow+i] = 0.0f;
        }
		for (unsigned int i = num_pixels; i < num_pixels+image_width*4; ++i)
        {
            out[fourthLastRow+i] = 0.0f;
        }
    }
    PROFILING_END()

	// Fill param_ctx struct with needed variables
	ctx_sobel_struct param_ctx __attribute__ ((aligned(128)));
    param_ctx.imageWidth = image_width;
	
    // Create task and task descriptor handles
    alf_task_handle_t task_handle;
    alf_task_desc_handle_t td_handle;

    // Names of SPU objects and functions
    char library_name[] = "spu_programs.so";   
    char spu_image_name[] = "spu_sobel";
    char kernel_name[] = "kernel";

    // *** SINGLE TASK STARTS HERE *** 
    PROFILING_START("Sobel", "init-and-create")
    {
        // Create and configure the task descriptor
        alf_task_desc_create(alf_handle, ALF_ACCEL_TYPE_SPE, &td_handle);

        // Identify the name of the object library
        alf_task_desc_set_int64(td_handle, ALF_TASK_DESC_ACCEL_LIBRARY_REF_L, (unsigned long long)library_name);

        // Identify the name of the SPU image
        alf_task_desc_set_int64(td_handle, ALF_TASK_DESC_ACCEL_IMAGE_REF_L, (unsigned long long)spu_image_name);

        // Identify the name of the kernel
        alf_task_desc_set_int64(td_handle, ALF_TASK_DESC_ACCEL_KERNEL_REF_L, (unsigned long long)kernel_name);

        // Configure the memory requirements and add the task context
        alf_task_desc_set_int32(td_handle, ALF_TASK_DESC_MAX_STACK_SIZE, 4096);

        // Configure memory for the input buffer (array_a)
        alf_task_desc_set_int32(td_handle, ALF_TASK_DESC_WB_IN_BUF_SIZE, image_width*(2+standardBlockSize)*sizeof(float));

        // Configure memory for the output buffer (array_b)
        alf_task_desc_set_int32(td_handle, ALF_TASK_DESC_WB_OUT_BUF_SIZE, image_width*2*standardBlockSize*sizeof(float));    

        // Configure memory for the work block parameter context
        alf_task_desc_set_int32(td_handle, ALF_TASK_DESC_WB_PARM_CTX_BUF_SIZE, sizeof(ctx_sobel_struct));

        // Create the task for cyclic work block distribution
        int ret;
        if ((ret = alf_task_create(td_handle, NULL, 0, 0, 0, &task_handle)) < 0) 
        {
            fprintf(stderr, "alf_task_create failed, ret=%d\n", ret);
            return 1;
        }
    }
    PROFILING_END()

    for (unsigned int i=0; i<num_blocks; i++) 
    {
        PROFILING_START("Sobel", "wrkblk-create-and-enqueue")
        {
            if (i != num_blocks-1)
    		{
    			param_ctx.numLines = standardBlockSize;
    		}
    		else
    		{
    			param_ctx.numLines = finalBlockSize;
    		}
    		
    		// Create the work block
            alf_wb_create(task_handle, ALF_WB_SINGLE, 0, &wb_handle);
            alf_wb_parm_add(wb_handle, (void *)(&param_ctx), sizeof(param_ctx), ALF_DATA_BYTE, 0);

            // DTL: Store in an input buffer
            alf_wb_dtl_begin(wb_handle, ALF_BUF_IN, 0);
            alf_wb_dtl_entry_add(wb_handle, &in[image_width*3+standardBlockSize*i*image_width], 
                    image_width*(2+param_ctx.numLines), ALF_DATA_FLOAT);
            alf_wb_dtl_end(wb_handle);

            // DTL: Tranfer the edge intensity portion of the output buffer into in the front half of the output array
            alf_wb_dtl_begin(wb_handle, ALF_BUF_OUT, 0);
            alf_wb_dtl_entry_add(wb_handle, &out[image_width*4+standardBlockSize*i*image_width], 
                    image_width*param_ctx.numLines, ALF_DATA_FLOAT);
            alf_wb_dtl_end(wb_handle);
    		// DTL: Tranfer the edge direction portion of the output buffer into in the back half of the output array
    		 alf_wb_dtl_begin(wb_handle, ALF_BUF_OUT, sizeof(float)*image_width*standardBlockSize);
            alf_wb_dtl_entry_add(wb_handle, &out[image_width*4+standardBlockSize*i*image_width+num_pixels], 
                    image_width*param_ctx.numLines, ALF_DATA_FLOAT);
            alf_wb_dtl_end(wb_handle);

            // Enqueue the work block in the task
            alf_wb_enqueue(wb_handle);
        }
        PROFILING_END()
    }

    // Finalise the task
    alf_task_finalize(task_handle);

    PROFILING_START("Sobel", "task-wait")
    {
        // Wait for task to complete
        alf_task_wait(task_handle, -1);
    }
    PROFILING_END()

	// Destroy task and task descriptor handles
	alf_task_desc_destroy(td_handle);
    alf_task_destroy(task_handle);

    // *** SINGLE TASK ENDS HERE *** 

    return 0;
}

int spuBlur(float* in, float* out, unsigned int blockSize)
{
    // Configure number of workblocks and workblocks sizes
	unsigned int standardBlockSize, finalBlockSize;
	div_t divresult;
	divresult = div(static_cast<int>(image_height - 6), static_cast<int>(blockSize));
	if(divresult.rem == 0)
	{
		num_blocks = divresult.quot;
		standardBlockSize = blockSize;
		finalBlockSize = blockSize;
	}
	else
	{
		num_blocks = divresult.quot + 1;
		standardBlockSize = blockSize;
		finalBlockSize = divresult.rem;
	}

    printf( "SPU BLUR: StandardBlockSize/FinalBlockSize/NumBlocks = %d/%d/%d\n", 
            standardBlockSize, 
            finalBlockSize, 
            num_blocks ); 
    // Fill matrix one with the image and fill non-block rows with zeros
    PROFILING_START("Guassian", "init-and-create")
    {
        unsigned int thirdLastRow = num_pixels - image_width*3;
     	
        for (unsigned int i=0; i< image_width*3; ++i)
        {
            out[i] = 0;
        }

        for (unsigned int i=0; i< image_width*3; ++i)
        {
            out[thirdLastRow+i] = 0;
        }

    }
    PROFILING_END()

    // Fill param_ctx struct with needed variables
    ctx_blur_struct param_ctx __attribute__ ((aligned(128)));
    param_ctx.imageWidth = image_width;
	
    // Create task and task descriptor handles
    alf_task_handle_t task_handle;
    alf_task_desc_handle_t td_handle;

    // Names of SPU objects and functions
    char library_name[] = "spu_programs.so";   
    char spu_image_name[] = "spu_blur";
    char kernel_name[] = "kernel";
    
    // *** SINGLE TASK STARTS HERE *** 
    PROFILING_START("Guassian", "init-and-create")
    {
        // Create and configure the task descriptor
        alf_task_desc_create(alf_handle, ALF_ACCEL_TYPE_SPE, &td_handle);

        // Identify the name of the object library
        alf_task_desc_set_int64(td_handle, ALF_TASK_DESC_ACCEL_LIBRARY_REF_L, (unsigned long long)library_name);

        // Identify the name of the SPU image
        alf_task_desc_set_int64(td_handle, ALF_TASK_DESC_ACCEL_IMAGE_REF_L, (unsigned long long)spu_image_name);

        // Identify the name of the kernel
        alf_task_desc_set_int64(td_handle, ALF_TASK_DESC_ACCEL_KERNEL_REF_L, (unsigned long long)kernel_name);

        // Configure the memory requirements and add the task context
        alf_task_desc_set_int32(td_handle, ALF_TASK_DESC_MAX_STACK_SIZE, 4096);

        // Configure memory for the input buffer (array_a)
        alf_task_desc_set_int32(td_handle, ALF_TASK_DESC_WB_IN_BUF_SIZE, image_width*(6+standardBlockSize)*sizeof(float));

        // Configure memory for the output buffer (array_b)
        alf_task_desc_set_int32(td_handle, ALF_TASK_DESC_WB_OUT_BUF_SIZE, image_width*standardBlockSize*sizeof(float));    

        // Configure memory for the work block parameter context
        alf_task_desc_set_int32(td_handle, ALF_TASK_DESC_WB_PARM_CTX_BUF_SIZE, sizeof(ctx_blur_struct));

        // Create the task for cyclic work block distribution
        int ret;
        if ((ret = alf_task_create(td_handle, NULL, 0, 0, 0, &task_handle)) < 0) 
        {
            fprintf(stderr, "alf_task_create failed, ret=%d\n", ret);
            return 1;
        }  
    }
    PROFILING_END()
    
    for (unsigned short i=0; i<num_blocks; i++) 
    {
        PROFILING_START("Guassian", "wrkblk-create-and-enqueue")
        {
    		if (i != num_blocks-1)
    		{
    			param_ctx.numLines = standardBlockSize;
    		}
    		else
    		{
    			param_ctx.numLines = finalBlockSize;
    		}
    		
    		// Create the work block
            alf_wb_create(task_handle, ALF_WB_SINGLE, 0, &wb_handle);
            alf_wb_parm_add(wb_handle, (void *)(&param_ctx), sizeof(ctx_blur_struct), ALF_DATA_BYTE, 0);

            // DTL: store array_a in an input buffer
            alf_wb_dtl_begin(wb_handle, ALF_BUF_IN, 0);
            alf_wb_dtl_entry_add(wb_handle, &in[image_width*standardBlockSize*i], 
                    image_width*(6+param_ctx.numLines), ALF_DATA_FLOAT);
            alf_wb_dtl_end(wb_handle);

            // DTL: store array_b in an output buffer
            alf_wb_dtl_begin(wb_handle, ALF_BUF_OUT, 0);
            alf_wb_dtl_entry_add(wb_handle, &out[image_width*3+image_width*standardBlockSize*i], 
                    image_width*param_ctx.numLines, ALF_DATA_FLOAT);
            alf_wb_dtl_end(wb_handle);

            // Enqueue the work block in the task
            alf_wb_enqueue(wb_handle);
        }
        PROFILING_END()
    }

    // Finalise the task
    alf_task_finalize(task_handle);
    PROFILING_START("Guassian", "task-wait")
    {
        // Wait for task to complete
        alf_task_wait(task_handle, -1);
    }
    PROFILING_END()

    // Destroy task and task descriptor handles
    alf_task_desc_destroy(td_handle);
    alf_task_destroy(task_handle);

    // *** SINGLE TASK ENDS HERE *** 

    return 0;
}

int loadImage(  const char* imageFilename, std::vector<unsigned char> &imageBuffer, 
                float** array_a, float** array_b, bool arraysNeedAllocated)
{
	std::string temp = imageFilename;
    std::string temppng = temp + ".png";
	
    unsigned int error = 0;
    PROFILING_START("LoadImage", "decode")
    {
        // Wait for task to complete
        printf(Magenta_"Decoding %s\n"Revert_, temppng.c_str());
        imageBuffer.clear();
        error = lodepng::decode(imageBuffer, image_width, image_height, 
                                temppng.c_str(), LCT_GREY, 8);
    }
    PROFILING_END()

	if(error != 0)
	{
		std::cout   << "decoder error for " << temppng.c_str() << error << ": " 
                    << lodepng_error_text(error) << std::endl;
		return 1;
	}
    num_pixels = image_width * image_height;
    printf("%s info:\n"
            Green_
            "\t Width: %d\n"
            "\t Height: %d\n"
            "\t Number of Pixels: %d\n"
            Revert_,
            temppng.c_str(), image_width, image_height, num_pixels);
	
	if (!arraysNeedAllocated)
	{
		// do nothing
	}
	else
	{
		*array_a = new float[num_pixels*2];
		*array_b = new float[num_pixels*2];	
	}
	
	for (unsigned int i=0; i<num_pixels; ++i) 
	{
		(*array_a)[i] = static_cast<float>(imageBuffer[i]);
	}
		
	return 0;
}

int saveImage(const char* filename, float* rawPixels)
{
	 // Fill the image array with the data now in array_b
    PROFILING_START("SaveImage", "copy-rawPixels")
    {
        image.clear();
        image.reserve(num_pixels);
        for (unsigned int i=0; i<num_pixels; ++i){
            image.push_back(static_cast<unsigned char>(rawPixels[i]));
        }
    }
    PROFILING_END()
    std::string temp;
	temp = filename;
    temp += ".png";
    PROFILING_START("SaveImage", "encode")
    {
        // Encode the image
        printf(Magenta_"Ecoding %s\n\n"Revert_, temp.c_str());

        std::vector<unsigned char> buffer __attribute__ ((__aligned__(128)));
        lodepng::State state;
        state.info_png.color.colortype = LCT_GREY;
        state.info_png.color.bitdepth = 8;
        state.info_raw.colortype = LCT_GREY;
        state.info_raw.bitdepth = 8;

        int error = lodepng::encode(buffer, image, static_cast<unsigned int>(image_width), 
                                    static_cast<unsigned int>(image_height), state);
        // if there's an error, display it
        if(error != 0){
            std::cout << "encoder error for " << temp.c_str() << error << ": " 
            << lodepng_error_text(error) << std::endl;
            return 1;
        }
        else
            lodepng::save_file(buffer, temp.c_str());
    }
    PROFILING_END()
	return 0;
}

int main(int argc, char **argv) 
{
    // Intro text
    printf(	Green_ 
            "Games Console Development Coursework\n"
            "> Floyd Chitalu (B00203579)\n"
            "> Shane Graham (B00239498)\n"
            "> Andrew Devlin (B00216209)\n\n" 
            Revert_);
	
    g_kmeans_seed_value = (int)time(NULL);
 
	// Command Line argument parsing
	ArgvParser parser;
	parser.setIntroductoryDescription("GCD Coursework - A program to detect regions of interest given an input image");
	parser.setHelpOption("h", "help", "Prints the help page");
	parser.addErrorCode(0, "Sucess");
	parser.addErrorCode(1, "Error");
	
	parser.defineOption("filename", "Name of input file minus it's index. Default value: AT3_1m4_", ArgvParser::OptionRequiresValue);
	parser.defineOption("filenameCount", "Number of filename indexes to run. Default value: 1", ArgvParser::OptionRequiresValue);
	parser.defineOption("spuCount", "Name of spu's to utilise. Default value: 6", ArgvParser::OptionRequiresValue);
	parser.defineOption("blockSize", "Name of lines to output per spu workblock. Default value: 1", ArgvParser::OptionRequiresValue);
	parser.defineOption("ncent", "Number of centroids to generate for kmeans clustering. Default is 6", ArgvParser::OptionRequiresValue);
	parser.defineOption("niter", "Number of permitted iterations in which to run kmeans clustering. Default is 100", ArgvParser::OptionRequiresValue);
	parser.defineOption("seed", "Value used for random kmeans centroid value generation. Usefull if you wish to repeat results", ArgvParser::OptionRequiresValue);
	parser.defineOption("nspe_kmeans", "Number of SPE threads used to execute the kmeans clustering algorithm. Maximum is 6 on PS3.", ArgvParser::OptionRequiresValue);
	parser.defineOption("noOutput", "Disables program PNG ouput, useful when used with --benchmark to speed up iterations.", ArgvParser::NoOptionAttribute);
	parser.defineOption("benchmark", "Number of iterations to run entire program on input images, used to profile performance. Default value 1", ArgvParser::OptionRequiresValue);
	int result = parser.parse(argc, argv);
	
	if (result!= ArgvParser::NoParserError){
		cout << parser.parseErrorDescription(result);
		exit(1);
	}
	else{
		if (parser.foundOption("filename")){
			filename = parser.optionValue("filename");
		}
		if (parser.foundOption("filenameCount")){
			filenameCount = static_cast<unsigned int>(
                std::strtol(parser.optionValue("filenameCount").c_str(),NULL,10));
		}
		if (parser.foundOption("spuCount")){
			numAccel = static_cast<unsigned int>(
                std::strtol(parser.optionValue("spuCount").c_str(),NULL,10));
		}
		if (parser.foundOption("blockSize")){
			outputBlockSize = static_cast<unsigned int>(
                std::strtoul(parser.optionValue("blockSize").c_str(),NULL,10));
        }
        if (parser.foundOption("ncent")){
            g_num_kmeans_centroids = static_cast<int>(
                std::strtoul(parser.optionValue("ncent").c_str(),NULL,10));
        }
        if (parser.foundOption("niter")){
            g_num_kmeans_iterations = static_cast<int>(
                std::strtoul(parser.optionValue("niter").c_str(),NULL,10));
        }
        if (parser.foundOption("seed")){
            g_kmeans_seed_value = static_cast<int>(
                std::strtoul(parser.optionValue("seed").c_str(),NULL,10));
        }
        if (parser.foundOption("nspe_kmeans")){
            g_num_kmeans_threads = static_cast<int>(
                std::strtoul(parser.optionValue("nspe_kmeans").c_str(),NULL,10));
        }
		if (parser.foundOption("noOutput")){
			hasOutput = false;
		}
		if (parser.foundOption("benchmark")){
			benchmarkIterations = static_cast<unsigned int>(std::strtoul(parser.optionValue("benchmark").c_str(),NULL,10));
		}
	}
	
	// Display list of image filenames
	char temp[3];
	for (unsigned int i = 0; i < filenameCount; ++i){
		sprintf (temp,"%s%02u", filename.c_str(), i+1);
		filenames[i] = temp;
		printf("Filenames%d: %s \n",i,filenames[i].c_str());
	}
	
    // Initialize the alf handle and set number of accelerators
    alf_init(NULL, &alf_handle);

    unsigned int maxAcell;
    alf_query_system_info(	alf_handle, 
            ALF_QUERY_NUM_ACCEL, 
            ALF_ACCEL_TYPE_SPE, 
            &maxAcell);
    if (numAccel <= maxAcell){
        alf_num_instances_set(alf_handle, numAccel);
    }
    else{
        alf_num_instances_set(alf_handle, maxAcell);
    }

    printf("Number of SPU's active: %d\n", numAccel);

    std::cout << std::endl;

    // Call spu blur function to create task, enqueue work blocks, finalise 
    //task, wait for task to complete, and destroy task.
	float* array_a __attribute__ ((__aligned__(128)));
	float* array_b __attribute__ ((__aligned__(128)));

    std::string outputFilename;
	for(unsigned int iterations = 0; iterations < benchmarkIterations; ++iterations){
		for(unsigned int i=0; i < filenameCount; ++i){ 
			// load image into imagebuffer, allocate memory for float arrays, copy imagebuffer into array_a
			if (i != 0)
				loadImage(filenames[i].c_str(), image, &array_a, &array_b, false);
			else
				loadImage(filenames[i].c_str(), image, &array_a, &array_b, true);
			
			// run spuBlur on array_a and store the output in array_b
			spuBlur(array_a, array_b, outputBlockSize);
					
			// save a copy of array_b to a png
			outputFilename = filenames[i] + "_5x5blur";
			if (hasOutput) saveImage(outputFilename.c_str(), array_b);
			
			// run spuSobel on array_b and store the output in array_a
			spuSobel(array_b, array_a, outputBlockSize);
					
			// save a copy of the intensity portion of array_a to a png
			outputFilename = filenames[i] + "_sobel";
			if (hasOutput) saveImage(outputFilename.c_str(), array_a);
			
			// save a copy of the direction portion of array_a to a png
			outputFilename = filenames[i] + "_directions";
			if (hasOutput) saveImage(outputFilename.c_str(), &array_a[num_pixels]);
			
			// run spuNonMaxSuppression on array_a and store the output in array_b
			spuNonMaxSuppression(array_a, array_b, outputBlockSize);
			
			// save a copy of array_b to a png
			outputFilename = filenames[i] + "_NMS";
			if (hasOutput) saveImage(outputFilename.c_str(), array_b);
			
			// Run Hysteresis for a number of iterations to refine edges.
			for(unsigned int j = 0; j <= 15; ++j) // always make this end in an odd to prevent issues with the doublebuffer
			{
				if (j % 2 == 1){	
					// run spuHysteresis on array_b and store the output in array_a
					spuHysteresis(array_a, array_b, 105, 120, outputBlockSize);
				}
				else{
					// run spuHysteresis on array_a and store the output in array_b
					spuHysteresis(array_b, array_a, 105, 120, outputBlockSize);
				}
			}
			// run spuHysteresis once more on array_b and store the output in 
            //array_a and only output a mask of 0 or 255
			spuHysteresis(array_b, array_a, 255, 255, outputBlockSize);
			// save a copy of array_b to a png
			outputFilename = filenames[i] + "_Hysteresis";

			if (hasOutput) saveImage(outputFilename.c_str(), array_a);

            printf("\x1B[38;5;3m\x1B[48;5;3m|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"Revert_"\n");
            g_pstats["K-means"].clear();
    		PROFILING_START("K-means", "total procedure"){
                //count number of pixels that are on to signify 
                //data entry points for k-means
                for(unsigned int j =0; j < NUM_PIXELS; ++j){
                    if(array_a[j] == 255)
                        ++num_ON_pixels;             
                }
                
                PROFILING_START("K-Means", " cell quantity evaluation"){
                    /*run kmeans to count the number of cells in image*/
                    evaluate_cell_quantity(argc, argv, array_a, num_ON_pixels);
                }PROFILING_END();
                
                PROFILING_START("K-means", "write cluster data"){
                    // save a modified version of array_a with data signifing
                    // bounding boxes around cells
			        outputFilename = filenames[i] + "_ROI";
			        if (hasOutput) saveImage(outputFilename.c_str(), array_a);
                }PROFILING_END();

            }PROFILING_END();
            printf("\x1B[38;5;3m\x1B[48;5;3m|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"Revert_"\n");

            num_ON_pixels = 0;
		}//for(unsigned int i=0; i < filenameCount; ++i)

		// array cleanup
		delete[] array_a;
		delete[] array_b;
	}//for(unsigned int iterations = 0; iterations < benchmarkIterations; ++iterations)
   
    alf_exit(alf_handle, ALF_EXIT_POLICY_FORCE, 0);

    display_profiling_times();

    return 0;
}



