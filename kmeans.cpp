#include "kmeans.h"
#include <malloc_align.h> //_malloc_align

#define EVENTS_TOTAL (12)
#define FOREVER (-1)

#define EA_EBASE_i (0)
#define EA_CBASE_i (6)
#define EA_ATTRIB (12)
#define EA_ATOMIC_VAR (13)
#define EA_ITER_VAR (14)

#define CENTROID_COUNT (0)
#define ENTRY_COUNT (1)
#define SPU_IDX (2)
#define SPE_COUNT (3)
#define ITER_COUNT (4)

/*
 * SPE Program handle
 * */
extern spe_program_handle_t spu_kmeans;

std::vector<spe_context_ptr_t> g_SPE_contexts;
std::vector<pthread_t> g_threads;

entry_data_t e_mem __attribute__((aligned(16)));
entry_data_t c_mem __attribute__((aligned(16)));

int g_num_entries, g_eload, g_espill, g_cload, g_cspill;

volatile int g_consumed_iterations __attribute__((aligned(128)));

//event handlers to manage dependencies...
std::vector<spe_event_handler_ptr_t> event_handlers;
std::vector<ppu_thread_data_t> compute;
spe_event_unit_t events[EVENTS_TOTAL];

/*
 *Atomic sync values
 * */
volatile atom_t at __attribute__((aligned(128))); 

volatile int cashier_var __attribute__((aligned(128)));
volatile int cashier_mutex_var __attribute__((aligned(128)));
volatile int served_mutex_var __attribute__((aligned(128)));

volatile int num_served __attribute__((aligned(128)));
volatile int iteration_count __attribute__((aligned(128)));
volatile int association_change_count __attribute__((aligned(128)));

int g_num_kmeans_centroids = 12,
    g_num_kmeans_iterations = 100,
    g_kmeans_seed_value = 0,//initialised later
    g_num_kmeans_threads = 1;

bool prog_run_atleast_once = false;

void* ppu_thread_func(void* arg)
{
    ppu_thread_data_t *ptd = (ppu_thread_data_t*)arg;

    spe_stop_info_t stop_info;
    unsigned int entry = SPE_DEFAULT_ENTRY;

    printf(	Cyan_ "~SPE("White_"%d"Cyan_") Context Thread "Green_"Launch."Revert_"\n",       
            ptd->cb.attribs[SPU_IDX]);
    
    //Run...
    int ret = spe_context_run(	ptd->spe_id, 
					            &entry, 
					            0, 
					            /*
									note: on spe context invocation we only send 
									the address to our array containing the effective addresses
									of our data. individual spus take care of pulling
									the data at the correct offsets etc.
					            */
					            &ptd->cb.ea_block, 
					            NULL, 
					            &stop_info); 

    /*
     *we wait on only two events for every SPU thread, hence the offset
     * */
    int offset = 2 * ptd->cb.attribs[SPU_IDX];
    int event_count = spe_event_wait(	event_handlers[ptd->cb.attribs[SPU_IDX]],
							            &events[offset],
							            2,
							            FOREVER);	

    for(unsigned short i(0); i < event_count; ++i)
    {
        if(__builtin_expect(events[offset + i].events & SPE_EVENT_SPE_STOPPED, 
                            MOSTLY_TRUE))
        {
            printf(Cyan_ "~SPE("White_"%d"Cyan_")  Context Thread "Red_"Stop."Revert_"\n", 
                ptd->cb.attribs[SPU_IDX]);
            spe_stop_info_read(events[offset + i].spe, &stop_info);
            if (stop_info.stop_reason != 1)
            {
                switch(stop_info.stop_reason)
                {
                    case 1:	printf(Green_ "SPE_EXIT" Revert_ "\n");	break;
                    case 2:	printf(Yellow_ "SPE_STOP_AND_SIGNAL" Revert_ "\n");	break;
                    case 3:	printf(Yellow_ "SPE_RUNTIME_ERROR" Revert_ "\n");	break;
                    case 4:	printf(Yellow_ "SPE_RUNTIME_EXCEPTION" Revert_ "\n"); break;
                    case 5: printf(Yellow_ "SPE_RUNTIME_FATAL" Revert_ "\n");break;
                    case 6:	printf(Yellow_ "SPE_CALLBACK_ERROR" Revert_ "\n");	break;
                }
            }

            if(__builtin_expect(stop_info.stop_reason != 1, MOSTLY_FALSE))
            {
                printf(">>SPE Exit Code: %d\n", stop_info.result.spe_exit_code);
            }
        }
    }
    
    ASSERT_TRUE(ret >= 0, "failed to execute thread %d", ret);
    pthread_exit(NULL);
}

void init_kmeans(float* raw_pixel_data)
{   
    /*
     *Allocate memory for centroids and data points
     * */
    printf(Yellow_ "STATUS: " Revert_ "Allocate resources.\n\n");
    int nspe = g_num_kmeans_threads;
    for(int /*spu*/idx=0; idx < g_num_kmeans_threads; ++idx)
    {
        //data points
        e_mem.ptrs[idx] = new entry_t[g_eload + (idx == nspe -1 ? g_espill : 0)];
        /*posix_memalign( (void**)(&e_mem.ptrs[idx]), 
                        128, 
                        sizeof(entry_t) * (g_eload + (idx == nspe -1 ? g_espill : 0)));*/
        ASSERT_TRUE(e_mem.ptrs[idx] != NULL, 
                    "failed to allocate memory for entries: %d", idx);
        printf( "PPE [Data-Points] Pointer Memory-Segment(%d) address: %p\n",
                idx, e_mem.ptrs[idx]);
        
        //centroids
        c_mem.ptrs[idx] = new centroid_t[g_cload + (idx == nspe -1 ? g_cspill : 0)];
        /*posix_memalign( (void**)(&c_mem.ptrs[idx]), 
                        128, 
                        sizeof(centroid_t) * (g_cload + (idx == nspe -1 ? g_cspill : 0)));*/
        ASSERT_TRUE(c_mem.ptrs[idx] != NULL, 
                    "failed to allocate memory for centroids: %d", idx);
        printf( "PPE [Centroids] Pointer Memory-Segment(%d) address: %p\n", 
                idx, c_mem.ptrs[idx]);
    }
    printf("\n");
    /*
       translates value from single array flat-index to image pixel
       coordinate (x, y)
       */
    struct
    {
        coord_t operator()(unsigned int &flat_index)
        {
            coord_t out;
            out._u.x = (unsigned int)(flat_index % IMAGE_WIDTH);
            out._u.y = (unsigned int)(flat_index / IMAGE_WIDTH);	
            return out;
        }
    }to_coord;

    printf(Yellow_ "STATUS: " Revert_ "Extract Data-Points From Raw Input-Image Ptr.\n");
    PROFILING_START("K-means", "entries-init"){
        int index = 0;
        int elem = 0;
        for(unsigned int i(0); i < NUM_PIXELS; ++i)
        {
            /*
                *note: this is pretty much the same comparison
                *done in main.cpp difference being that
                *we now store into a different array the data representing 
                *On pixels. 
            */
            bool pixel_is_on = raw_pixel_data[i] == 255;
            if(__builtin_expect(pixel_is_on, MOSTLY_FALSE))//most pixels are black
            {
                int idx = (index < g_eload ? 0 : (index / g_eload));
                if(__builtin_expect(idx >= g_num_kmeans_threads, MOSTLY_FALSE))
                { 
                    idx = g_num_kmeans_threads - 1;
                }

                //tranform "i" to 2d pixel coord
                coord_t coord = to_coord(i);
                
                entry_t* entry = &e_mem.ptrs[idx][elem];
                entry->_u.x = coord._u.x;
                entry->_u.y = coord._u.y; 
                entry->_u.id = -1;
                entry->_u.m = -1;

                ++elem;
                if(__builtin_expect(elem >= (g_eload + (idx == nspe -1 ? g_espill : 0)), 
                                    MOSTLY_FALSE))
                {
                    elem =0;
                }

                ++index;
            }
        }
    }PROFILING_END();

    /*
     *Alocate memory to hold centroid data
     * */
    
    printf(Yellow_ "STATUS: " Revert_ "Determine Initial Centroid State.\n");
    PROFILING_START("K-means", "centroids-init"){
        int elem = 0;
        for (int i(0u); i < g_num_kmeans_centroids; ++i)
        {
            int idx = (i < g_cload ? 0 : (i / g_cload));
                
            if(__builtin_expect(idx >= g_num_kmeans_threads, MOSTLY_FALSE))
            {
                idx = g_num_kmeans_threads - 1;
            }

            centroid_t* centroid = &c_mem.ptrs[idx][elem];

            centroid->_u.x = rand_range(0, IMAGE_WIDTH - 1);
            centroid->_u.y = rand_range(0, IMAGE_HEIGHT - 1);
            //failure to initialise this to zero causes horror if using "++"
            centroid->_u.m = 0;
            centroid->_u.id = (i +1) * 100;

            ++elem;
            if(__builtin_expect(elem >= (g_cload + (idx == nspe -1 ? g_cspill : 0)), 
                                MOSTLY_FALSE))
            { 
                elem =0;
            }
        }
    }PROFILING_END();

    //---
    compute.resize(g_num_kmeans_threads);

    /*
     * set up atmoic variables, if running on multiple SPUs > 1
     * */
    if(__builtin_expect(g_num_kmeans_threads > 1 && !prog_run_atleast_once, MOSTLY_TRUE))
    {
        printf(Yellow_ "STATUS: " Revert_ "Setup ATOMIC-sync state.\n");
        at.cashier = (cond_ea_t)&cashier_var;
        //at.cashier_mutex = (mutex_ea_t)&cashier_mutex_var;
        //at.served_mutex = (mutex_ea_t)&served_mutex_var;
        
        at.served_addr = (address_t)&num_served;
        at.iter_cnt_addr = (address_t)&iteration_count;
        at.chng_cnt_addr = (address_t)&association_change_count;

        /*
            For inter-process synchronization, a condition  variable  must
            be initialized once, and only once, before use.
        */
        cond_init(at.cashier); 
        //mutex_init(at.cashier_mutex);
        //mutex_init(at.served_mutex);

        cond_init(at.iter_cnt_addr);
        cond_init(at.chng_cnt_addr);
        cond_init(at.served_addr);
    }

    num_served = 0; 
    iteration_count=0; 
    association_change_count=0;
}

/*
   returns global mean distance
   */
bool run_kmeans(void) 
{
    bool converged(false);

    printf(Yellow_ "STATUS: " Revert_ "Config SPE Kernel ARG-POINTER\n");
    /*
       here we set all SPE argument data, that is the effective address
       required for DMA transfers.
    */
    for(unsigned char i(0u); i < g_num_kmeans_threads; ++i)
    {
        PROFILING_START("K-means", "init-SPE-input"){
            compute[i].spe_id = g_SPE_contexts[i];

            //number of data points that will be operated on using kmeans
            compute[i].cb.attribs[ENTRY_COUNT] = g_num_entries;
            //value indicating an SPU's assigned index value,
            //this vakue is use within the spu's in determining the
            //work loads
            compute[i].cb.attribs[SPU_IDX] = i;

            //number of active spu threads, see above for reason
            compute[i].cb.attribs[SPE_COUNT] = g_num_kmeans_threads;

            //the number of centroids used within the algorithm, 
            //this also represents the size of the "centroids" array
            compute[i].cb.attribs[CENTROID_COUNT] = g_num_kmeans_centroids;

            compute[i].cb.attribs[ITER_COUNT] = g_num_kmeans_iterations;

            for (int j = 0; j < g_num_kmeans_threads; ++j)
            {
                //address of entries pointer at offset
                compute[i].cb.ea_block.v[EA_EBASE_i + j] = (address_t)e_mem.ptrs[j];
                //address of centroids pointer at offset
                compute[i].cb.ea_block.v[EA_CBASE_i + j] = (address_t)c_mem.ptrs[j];
            }

            compute[i].cb.ea_block.v[EA_ATTRIB] = (address_t)compute[i].cb.attribs;
            compute[i].cb.ea_block.v[EA_ITER_VAR] = (address_t)&g_consumed_iterations;

            if(__builtin_expect(g_num_kmeans_threads > 1, MOSTLY_TRUE))
            {
                compute[i].cb.ea_block.v[EA_ATOMIC_VAR] = (address_t)&(at);
            }
        }PROFILING_END();
    }

    PROFILING_START("K-means", "thread-launch-exec-wait"){
        //this is where we create our threads which subsequently invoke the
        //spe_context_run function
        printf(Yellow_ "STATUS: " Revert_ "Thread Launch\n");
        for(unsigned char i(0u); i < g_num_kmeans_threads; ++i)
        {
            PROFILING_START("K-means", "SPE-pthread_create"){
                int ret = pthread_create(	&g_threads[i], 
                                            NULL, 
                                            ppu_thread_func,//spu context launcher  
                                            (void*)&compute[i]);//per-spu arg data

                ASSERT_TRUE(ret == 0, "failed to launch kmeans thread: %d", i);
            }PROFILING_END();
        }

        //Here, after we effectively spin of each thread that represents
        //an spu context, we simply spinloop-wait for all atomic updates to insure 
        //data reads and writes are properly synchronised...
        //only if the number of SPUs running is greater than
        //one do we need to be synchronising data reads and writes
        //for a successful kmeans iteration
        int nspe = g_num_kmeans_threads;
        int niter = g_num_kmeans_iterations;

        PROFILING_START("K-means", "SPE execution"){
            if(__builtin_expect(nspe > 1, MOSTLY_TRUE)) 
            {
                printf(Yellow_ "STATUS: " Revert_ "Orchestrating Inter-SPE Proc Mem-Sync\n");

                while((atomic_read((atomic_ea_t)at.iter_cnt_addr) <= (niter - 1))) //not reached last iteration
                {
                    //--SPE: evaluate_entry_assignment();--//

                    //...now wait for memory syncronisation between SPEs
                    //i.e. this spinloop waits for SPEs to share with others the
                    //[ENTRIES/DATA-POINTS MEMORY] they have each updated/processed.
                    {
                        //tell waiting SPE processes to continue i.e. they have all 
                        //updated global memory and can now get portions processed by other
                        //SPEs ito their local stores
                        while(atomic_read((atomic_ea_t)at.served_addr) != nspe);
                        //set value to zero so that it can be be by the next sync phase
                        atomic_set((atomic_ea_t)at.served_addr, 0);
                    }
                    //mutex_unlock(at.cashier_mutex);

                    //remember for kmeans, convergence is recognised when there is no 
                    //change in data-point to centroid affiliation, after an iteration.
                    //here we atomically read the value reported by SPEs if it is zero,
                    //then we have convergence else keep looping. 
                    if(__builtin_expect(atomic_read((atomic_ea_t)at.chng_cnt_addr) == 0, 
                                        MOSTLY_FALSE)) 
                    {
                        converged = true;  break;
                    }

                    //--SPE: evaluate_centroid_change();...--//

                    //now wait again for memory synchronisation as SPEs share there 
                    //portions of updated [CENTROID MEMORY].
                    { 
                        while(atomic_read((atomic_ea_t)at.served_addr) != nspe);
                        // cond_signal(at.cashier);
                        atomic_set((atomic_ea_t)at.served_addr, 0);
                    }              
                }
            }
            
            printf(Yellow_ "STATUS: " Revert_ "PPE Wait for SPE Threads\n");
            //force wait on all threads/ SPUS to complete
            for(unsigned char i(0u); i < g_num_kmeans_threads; ++i)
            { 
                PROFILING_START("K-means", "SPE-pthread_join"){
                    int ret = pthread_join(	g_threads[i], NULL);
                    ASSERT_TRUE(ret == 0, "failed to join kmeans thread: %d", i);
                }PROFILING_END();   
            }
        }PROFILING_END();       
    }PROFILING_END();

    converged = (g_consumed_iterations < g_num_kmeans_iterations -1) ? true : false; 

    printf( Yellow_ "STATUS: " Revert_ "K-Means Complete\n\n"
            Magenta_ "RESULT:\t%s%s" Revert_ "\n"
            "\tIterations Consumed: " Magenta_ "%d" Revert_ "\n\n",
            converged ? Green_ : Red_,
            converged ? "Converged Successfully!" : "Failed To Converge!",
            g_consumed_iterations);

    return converged;
}

void make_cluster_AABBs( float* raw_pixel_data)
{
    printf(Yellow_ "STATUS: " Revert_ "Generate AABBs.\n");
    int celem = 0, eelem = 0;
    int cidx =0, eidx = 0;
    int nspe = g_num_kmeans_threads;
    for(int c = 0; c < g_num_kmeans_centroids; ++c)
	{   
        cidx = (c < g_cload) ? 0 : (c / g_cload);
        if(__builtin_expect(cidx >= g_num_kmeans_threads, MOSTLY_FALSE))
        {
            cidx = g_num_kmeans_threads - 1;
        }

        centroid_t* centroid = &c_mem.ptrs[cidx][celem]; 

        //bounding box...
        coord_t min, max;
        min._u.x = IMAGE_WIDTH; max._u.x = 0;
        min._u.y = IMAGE_HEIGHT; max._u.y = 0;
        
        /*
         *loop for every data-point associated with the "centroid"
         *to find most maximum and minimum values.
         * */
        eelem = 0;
        for(int e = 0; e < g_num_entries; ++e)
        {
            eidx = (e < g_eload) ? 0 : (e / g_eload);
            if(__builtin_expect(eidx >= g_num_kmeans_threads, MOSTLY_FALSE))
            { 
                eidx = g_num_kmeans_threads - 1;
            }

            entry_t* entry = &e_mem.ptrs[eidx][eelem]; 
        
            if(__builtin_expect(centroid->_u.id == entry->_u.id, MOSTLY_FALSE))
            {
                if(entry->_u.x < min._u.x)
                    min._u.x = entry->_u.x;
                if(entry->_u.x > max._u.x)
                    max._u.x = entry->_u.x;

                if(entry->_u.y < min._u.y)
                    min._u.y = entry->_u.y;
                if(entry->_u.y > max._u.y)
                    max._u.y = entry->_u.y;                     
            }

            ++eelem;   
            if(__builtin_expect(eelem >= (g_eload + (eidx == nspe -1 ? g_espill : 0)), 
                                MOSTLY_FALSE))
            {
                eelem =0;
            }
        }

        ++celem;   
        if(__builtin_expect(celem >= (g_cload + (cidx == nspe -1 ? g_espill : 0)), 
                            MOSTLY_FALSE)) 
        {
            celem =0;
        }
        
        std::vector<coord_t> border_pixels;
        coord_t pixel; 
        //store upper and lower lines of bounding box 
        for(int x = min._u.x; x < max._u.x; ++x)
        {
            coord_t min_max_y;
            min_max_y.a[0] = min._u.y;
            min_max_y.a[1] = max._u.y;

            for(int i=0; i<2; ++i)
            {
                int y = min_max_y.a[i];
                pixel.a[0] =x; pixel.a[1] =y;
                border_pixels.push_back(pixel);
            }
        }
        
        //store left and right lines of bounding box 
        for(int y = min._u.y; y < max._u.y; ++y)
        {
            //printf("y: %d\n", y);
            coord_t min_max_x;
            min_max_x.a[0] = min._u.x;
            min_max_x.a[1] = max._u.x;

            for(int i=0; i<2; ++i)
            {
                int x = min_max_x.a[i];
                pixel.a[0] =x; pixel.a[1] =y;
                border_pixels.push_back(pixel);
            }
        }
       
       int d =0;
        //update array data
        for(std::vector<coord_t>::iterator i(border_pixels.begin());
            i != border_pixels.end();
            ++i)
        {
            //generate index from pixel coordinate
            int flat_idx = ((*i)._u.x + ((*i)._u.y * IMAGE_WIDTH));
            //turn on pixel if its not already
            //remember this modifies array_a created in the main function
            raw_pixel_data[flat_idx] = PIXEL_VAL_THRESH/(++d);
            d = (d > 4 ? 0 :d);
        }
    }
}

void evaluate_cell_quantity(int argc, 
							char *argv[], 
							float* raw_pixel_data, 
							unsigned int &num_recognised_pixel_data_points)
{
    ASSERT_TRUE (   g_num_kmeans_centroids >= g_num_kmeans_threads, 
                    "the number of centroids must be equal"
                    "to or greater than the number of spus\n"
                    "SPEs: %d centroids: %d\n", 
                    g_num_kmeans_threads, g_num_kmeans_centroids);

    ASSERT_TRUE (   g_num_kmeans_iterations >= 1, 
                    "the number of iterations must be equal"
                    "to or greater than 1");

    ASSERT_TRUE(g_num_kmeans_threads <= 6, "invalid number of SPEs specified");
	/*
		provide seed to system, used to spawn centroids in random positions
    */
    srand (g_kmeans_seed_value);

    g_num_entries = num_recognised_pixel_data_points;
    
    g_eload = g_num_entries / g_num_kmeans_threads;
    g_espill = g_num_entries % g_num_kmeans_threads;

    g_cload = g_num_kmeans_centroids / g_num_kmeans_threads;
    g_cspill = g_num_kmeans_centroids % g_num_kmeans_threads;

    
    printf( Cyan_ "\nKMEANS CLUSTERING\n" Revert_ "\n"
            "\nRunning on " Magenta_ "%d" Revert_ " SPE(s)\n"
            "\nSystem using SEED value: " Magenta_ "%d"Revert_"\n\n" 

            "Number of Data-Points: " Magenta_ "%d"Revert_"\n"
            "Data-Points Processed per-SPU: " Magenta_ "%d"Revert_"\n"
            "Data-Point spillover Quantity: " Magenta_ "%d"Revert_"\n\n"

            "Number of Centroids: " Magenta_ "%d"Revert_"\n"
            "Centroids processed per-SPU: " Magenta_ "%d"Revert_"\n"
            "Centroid spillover quantity: " Magenta_ "%d"Revert_"\n\n", 

            g_num_kmeans_threads,
            g_kmeans_seed_value,
            g_num_entries,
            g_eload,
            g_espill,
            g_num_kmeans_centroids,
            g_cload,
            g_cspill);
    
    printf(	"SPU load estimate: " Magenta_ "%lu"Revert_" Kb\n\n", 
            ((g_num_entries * sizeof(entry_t) +
            (g_num_kmeans_centroids * sizeof(centroid_t)) +
            (sizeof(unsigned short) * 8) + 
            (sizeof(entry_data_t) * 2) + 
            (sizeof(atom_t)) +
            (sizeof(address_t) * 14)
            ) / 1024) / g_num_kmeans_threads);

    g_threads.resize(g_num_kmeans_threads);
    
    printf( Yellow_ "STATUS: " Revert_ "K-Means Start\n");

    PROFILING_START("K-means", "initialisation"){
        init_kmeans(raw_pixel_data);
    }PROFILING_END();

    /*
     *display the initial centroid positions
     * */
    printf("\n" Cyan_ "INITIAL CENTROID POSITIONS"Revert_"\n\n");
    int idx =0, elem =0;
    int nspe = g_num_kmeans_threads;
    for(int c = 0; c < g_num_kmeans_centroids; ++c)
    {   
        idx = (c < g_cload) ? 0 : (c / g_cload);
        if(__builtin_expect(idx >= g_num_kmeans_threads, MOSTLY_FALSE))
        {
            idx = g_num_kmeans_threads - 1;
        }

        centroid_t* centroid = &c_mem.ptrs[idx][elem];
        ++elem;   
        if(__builtin_expect(elem >= (g_cload + (idx == nspe -1 ? g_cspill : 0)), 
                            MOSTLY_FALSE))
        {
            elem =0;
        }

	    printf("[%d]=(%d,%d)\t", centroid->_u.id, centroid->_u.x, centroid->_u.y);
        if(__builtin_expect(!((c+1) % 3), 0))
            printf("\n");
	}
    printf("\n\n");

    /*
       create context and load program for every SPE
       */
    printf(Yellow_ "STATUS: " Revert_ "Context Create.\n");
    for(unsigned char i(0); i< g_num_kmeans_threads; ++i)
    {
        PROFILING_START("K-means", "SPE-setup")
        {
            //Note: here we also enable event processing
            g_SPE_contexts.push_back(spe_context_create(SPE_EVENTS_ENABLE, NULL));
            ASSERT_TRUE(g_SPE_contexts.back() != NULL, "failed to create context: %d", i);

            //create and event handler and an event type for every context.
            event_handlers.push_back(spe_event_handler_create());

            spe_event_unit_t eu;
            eu.spe = g_SPE_contexts.back();
            //we only care about an SPU stopping
            eu.events = SPE_EVENT_SPE_STOPPED;

            //register event
            spe_event_handler_register(event_handlers.back(), &eu);

            //load program (every spu gets the same program)
            int status = spe_program_load(g_SPE_contexts.back(), &spu_kmeans);
            ASSERT_TRUE(status == 0, "falied to load program");
        }
        PROFILING_END();
    }

    /*
     *  here we run the kmeans algorthm and retreive a result signifying
     *  whether the algorithm achieved convergence or not.
     *  Convergence is when all the data points not longer change affiliation
     *  from one centroid to another following an iteration.
     * */
    bool converged; 
    PROFILING_START("K-means", "run"){
        converged = run_kmeans();
    }PROFILING_END();

    /*
     *display the results, after kmeans completion
     * */
    printf("\n" Cyan_ "FINAL CENTROID POSITIONS & CLUSTER REPRESENTATIONS"Revert_"\n");


    unsigned short total_associations = 0;

    idx =0;elem=0;
    for(int c = 0; c < g_num_kmeans_centroids; ++c)
    {   
        idx = (c < g_cload) ? 0 : (c / g_cload);
        if(__builtin_expect(idx >= g_num_kmeans_threads, MOSTLY_FALSE)) 
        {
            idx = g_num_kmeans_threads - 1;
        }

        centroid_t* centroid = &c_mem.ptrs[idx][elem];
        ++elem;   
        if(__builtin_expect(elem >= (g_cload + (idx == nspe -1 ? g_cspill : 0)), 
                            MOSTLY_FALSE))
        {
            elem =0;
        }
	    printf( "[%d](%d, %d)->" Yellow_ "%d\t" Revert_,
                centroid->_u.id,
	    		centroid->_u.x, centroid->_u.y, 
	    		centroid->_u.m);

        total_associations += centroid->_u.m;

        if(__builtin_expect(!((c+1) % 3), 0))
            printf("\n");
	}
    printf("\n");
    
    ASSERT_TRUE((total_associations == num_recognised_pixel_data_points), 
                "centroids quantity to kmeans data-point associations mismatch.\n"
                "value is: %d but is supposed to be %d", 
                total_associations, num_recognised_pixel_data_points);

    /*
     *modify input array to visualise kmeans clustering representation
     * */
    PROFILING_START("K-means", "visualise-ROI"){
        make_cluster_AABBs( raw_pixel_data);
    }PROFILING_END();

    printf(Yellow_ "STATUS: " Revert_ "Resource Deallocate.\n");
    /*
     *Free all acquired heap resources
     * for reasons unknown if i run with 6 spus, freeing heap
     resources causes a crash sometimes
     * */
    for(int idx=0; idx < g_num_kmeans_threads; ++idx)
    {
        delete (c_mem.ptrs[idx]); 
        c_mem.ptrs[idx] = NULL;
        delete (e_mem.ptrs[idx]); 
        e_mem.ptrs[idx] = NULL;
    }
    printf(Yellow_ "STATUS: " Revert_ "SPE Context(s) Destroy.\n");
    /*
     *Destroy SPE contexts
     * */
    for(unsigned char i = 0; i < g_SPE_contexts.size(); ++i)
    {
        ASSERT_TRUE(0 == spe_context_destroy(g_SPE_contexts[i]), 
                    "failed to destroy context: %d", i);

         ASSERT_TRUE(0 == spe_event_handler_destroy(event_handlers[i]), 
                    "failed to destroy event_handler: %d", i);
    }
    g_SPE_contexts.clear();
    event_handlers.clear();

    /*
        The  condition destroy functions destroy any state, but not the space, 
        associated with the condition variable.

       The cond_destroy() function destroys any state  associated  with  the  
       condition variable pointed to by cvp. The space for storing the condition 
       variable is not freed.
    */
    if (__builtin_expect(g_num_kmeans_threads > 1, 1))
    {
        /*ASSERT_TRUE(0 == cond_destroy(at.cashier), 
                    "failed to release conditional var: cashier");
        ASSERT_TRUE(0 == cond_destroy(at.iter_cnt_addr), 
                    "failed to release conditional var: iter_cnt_addr");
        ASSERT_TRUE(0 == cond_destroy(at.chng_cnt_addr), 
                    "failed to release conditional var: chng_cnt_addr");
        ASSERT_TRUE(0 == cond_destroy(at.served_addr), 
                    "failed to release conditional var: chng_cnt_addr");
        */
    }

    prog_run_atleast_once = true;
}

