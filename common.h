#ifndef __COMMON_H__
#define __COMMON_H__

/*
 * This file contains shared variables and function protytpes 
 * used by the PPE executable.
 * */

#include <signal.h>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <map>
#include <string>
#include <time.h> 
#include <limits.h>
#include <math.h>
#include <altivec.h>

/*
 * This header is required for all "spe_..." functions and all 
 * associated data types 
 * */
#include <libspe2.h>
/*
 *pthread library
 * */
#include <pthread.h>

/*
 *TODO: add doc-string
 * */
#include <ppu_intrinsics.h>

#define PRINT_ERRSTR(msg_on_failure, ...) fprintf(stderr, Red_ "> ASSERTION FAILED:\n" #msg_on_failure "\n", ##__VA_ARGS__);\
fprintf(stderr, "**errno string: "); print_errno();\
fprintf(stderr, "LINE: %d\nFUNCTION: %s\nFILE: %s\n\n" Revert_, __LINE__, __PRETTY_FUNCTION__, __FILE__);\
fflush(stderr); fflush(stdout);

#ifndef NDEBUG
/* nice little macro useful for debugging */
#define DEBUG_BUILD 1
#else //NDEBUG
#define DEBUG_BUILD 0
#endif

#if DEBUG_BUILD

// "raise(SIGINT);" raises breakpoint in gdb 
#define ASSERT_TRUE(condition, msg_on_failure, ...)\
do{\
if(__builtin_expect(!(condition), 0)){\
PRINT_ERRSTR(msg_on_failure, ##__VA_ARGS__);\
raise(SIGINT);\
}}while(0);

#else //#if DEBUG_BUILD

#define ASSERT_TRUE(condition, msg_on_failure, ...)\
do{\
if(__builtin_expect(!(condition), 0)){\
PRINT_ERRSTR(msg_on_failure, ##__VA_ARGS__);\
exit(EXIT_FAILURE);\
}}while(0);

#endif //#else (#if DE...)

/*
 *text colour configuration macros
 * */
#define __COL_CONFIG(c) "\033[0;3" #c ";40m"

#ifdef UNDEF_COL

#define Red_  
#define Green_  
#define Yellow_  
#define Blue_  
#define Magenta_  
#define Cyan_  
#define White_ 
#define Revert_ 

#else

#define Red_ __COL_CONFIG(1) 
#define Green_ __COL_CONFIG(2) 
#define Yellow_ __COL_CONFIG(3) 
#define Blue_ __COL_CONFIG(4) 
#define Magenta_ __COL_CONFIG(5) 
#define Cyan_ __COL_CONFIG(6) 
#define White_ __COL_CONFIG(7)
#define Revert_ "\033[0;0;0m"

#endif //UNDEF_COL

#define TICKS_PER_SECOND (79800000)

#define IMAGE_WIDTH (640)
#define IMAGE_HEIGHT (480)

#define NUM_PIXELS (IMAGE_WIDTH * IMAGE_HEIGHT)

#define PIXEL_VAL_THRESH (255.0f)

/*
	profiling macros	
*/
#define PROFILING_START(category_, name_)\
do{\
const char* icat = #category_; \
const char* iname = #name_; \
ull_t start_ = __mftb(); \
do

#define PROFILING_END() \
while(0);\
ull_t end_ = __mftb(); \
g_pstats[icat][iname].push_back(end_ - start_);\
}while(0);

#define MOSTLY_TRUE (1)
#define MOSTLY_FALSE (0)

/*
 *way too long...! ^_^ 
 * */
typedef unsigned long long ull_t;
typedef ull_t address_t;

/*
 *profile statistic instance 
 * */
typedef std::map<   std::string,//instance name 
                     std::vector<ull_t>//associated time stamps
                 >psi_t;
/*
 *key = string 
 *value = map
 * */
typedef std::map<   std::string,//category name
                    psi_t> pstat_t;

/*kmeans values.
 default values assigned in kmeans.cpp*/
extern int g_num_kmeans_centroids;
extern int g_num_kmeans_iterations;
extern int g_kmeans_seed_value;
extern int g_num_kmeans_threads;

extern pstat_t g_pstats;

extern void display_profiling_times(void);

extern void print_kmeans_cmd_help(void);

/*
 *writes the current value of "errno" to stderr
 * @defined in common.cpp
 * */
extern void print_errno(void);

/*
   generate a random integer within a specified range
   */
extern int rand_range(int min, int max);

/*
* Execution the Kmeans algorithm to determine the number of cells
*	
*/
extern void evaluate_cell_quantity( int argc, 
                                    char *argv[],
                                    float *raw_pixel_data,
                                    unsigned int& num_recognised_pixel_data);

/*
 *set up function unsed to initialise global program state
 *specific to PPE
 *@defined in main.cpp
 * */
extern void PPE_setup(int argc, const char* argv[]);

/*
 *effectively runs program indefinitly until termination
 *@defined in main.cpp
*/
extern void PPE_execute(void);

/*
 *PPE global state teardown
 *@defined in main.cpp
 * */
extern void PPE_teardown(void);

#endif //__COMMON_H__
