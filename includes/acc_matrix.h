#ifndef __matrix_add_h__
#define __matrix_add_h__

typedef struct _ctx_struct {
  unsigned int imageWidth;
} ctx_struct;

typedef struct _ctx_blur_struct {
  unsigned int imageWidth;
  unsigned int numLines;
} ctx_blur_struct;

typedef struct _ctx_sobel_struct {
  unsigned int imageWidth;
  unsigned int numLines;
} ctx_sobel_struct;

typedef struct _ctx_NMS_struct {
  unsigned int imageWidth;
  unsigned int numLines;
} ctx_NMS_struct;

typedef struct _ctx_hysteresis_struct {
  unsigned int imageWidth;
  unsigned int numLines;
  unsigned int highThreshold;
  unsigned int lowThreshold;
} ctx_hysteresis_struct;

#endif /* __matrix_add_h__ */
