#include "common.h"

void print_errno(void)
{
	switch(errno)
	{
		case ESRCH: fprintf(stderr, "ESRCH\n"); break;	
		case EINVAL: fprintf(stderr, "EINVAL\n");break;
		case EIO: fprintf(stderr, "EIO\n");break;
		case EFAULT: fprintf(stderr, "EFAULT\n");break;
		case EPERM: fprintf(stderr, "EPERM\n");break;
		default: fprintf(stderr, "UNKNOWN: %d\n", errno);break;
	};
}

/*
   generate a random integer within a specified range
   */
int rand_range(int min, int max)
{
    int r;
    const int range = max - min;
    const int buckets = RAND_MAX / range;
    const int limit = buckets * range;

    /* Create equal size buckets all in a row, then fire randomly towards
     * the buckets until you land in one of them. All buckets are equally
     * likely. If you land off the end of the line of buckets, try again. */
    do
    {
        r = rand();
    } while (r >= limit);

    return min + (r / buckets);
}

enum FTYPE { CONS=0, CSV };

void display_profiling_times(void)
{
    if(__builtin_expect(!g_pstats.size(), 0))
        return;

    typedef pstat_t::const_iterator iter_t;
    
    printf(Cyan_);

   struct{ 
       std::string category,  name;

       ull_t    instance_ticks, arith_mean_ticks,
                lowest_ticks, highest_ticks;
       double geo_mean;
       //used if printing stats on averaged profile
       unsigned int num_iterations;
       bool looped;
        
       void print(void)
       {
           this->operator()(CONS);
           this->operator()(CSV);
       }
    
    private:

       void operator()(unsigned char ftype)
       {
           switch(ftype)
           {
               case CONS:
                   if(__builtin_expect(!looped, 1))
                   {
                       printf( " ________________________________________________________________\n"
                               "|\tcategory: %s\n|\tinstance: %s\n"
                               "|\tclock-ticks: %lld\n|\tmicroseconds: %.2f\n"
                               "|\tmilliseconds: %.2f\n|\tseconds: %lld\n"
                               "|________________________________________________________________\n",
                               category.c_str(),
                               name.c_str(),
                               instance_ticks,
                               (instance_ticks / 79.8f),
                               (instance_ticks/ 79.8f) * 0.001f,
                               (instance_ticks/TICKS_PER_SECOND));	
                   }
                   else
                   {
                       printf( " ________________________________________________________________\n"
                               "|\tcategory: %s\n|\tinstance: %s\n"
                               "|\tmean-clock-ticks: %lld\n|\tmean-microsecs: %.2f\n"
                               "|\tmean-millisecs: %.2f\n|\tmean-secs: %lld\n"
                               "|\tgeometric-mean: %f\n"
                               "|\titerations: %u\n|\thighest-ticks: %lld\n|\tlowest-ticks: %lld\n"
                               "|________________________________________________________________\n",
                               category.c_str(),
                               name.c_str(),
                               arith_mean_ticks,
                               (arith_mean_ticks / 79.8f),
                               (arith_mean_ticks/ 79.8f) * 0.001f,
                               (arith_mean_ticks/TICKS_PER_SECOND),
                               geo_mean,
                               num_iterations,
                               highest_ticks,
                               lowest_ticks);
                   }
                   break;
               case CSV:
                    static bool printed_titles = false;
                    
                    const char* fname = "profile-stat.csv";
                    
                    freopen(fname, (!printed_titles ? "w" : "a"), stdout);

                    if(__builtin_expect(!printed_titles, 0))
                    {
                        printf( "INSTANCE,CATEGORY,AVERAGE-BASED,CLOCKTICKS,GEOMETRIC,"
                                "HIGH,LOW,MICROSECONDS,MILLISECONDS,SECONDS,ITERATIONS\n");
                        printed_titles = true;
                    }

                    printf( "%s,%s,%s,%lld,%.3f,%lld,%lld,%f,%f,%llu,%d\n",
                            name.c_str(),
                            category.c_str(),
                            (looped ? "true" : "false"),
                            (looped ? arith_mean_ticks : instance_ticks),
                            (looped ? geo_mean : 0.0),
                            (looped ? highest_ticks : 0),
                            (looped ? lowest_ticks : 0),
                            (( looped ? arith_mean_ticks : instance_ticks) / 79.8f),
                            (( looped ? arith_mean_ticks : instance_ticks)/ 79.8f) * 0.001f,
                            (( looped ? arith_mean_ticks : instance_ticks)/TICKS_PER_SECOND),
                            (looped ? num_iterations : 1));
                    freopen("/dev/tty", "a", stdout);
                break;    
           }
       }
   }printer;
    
    printf("\nPROFILING STATISTICS\n....................\n\n");
        
   //start by looping through categories of profiling data...
    for(	iter_t it(g_pstats.begin()); 
            it != g_pstats.end(); 
            ++it)
    {
        printer.category = it->first;
        psi_t instances = it->second;

        //for every category, loop through individual cases/instances
        for(psi_t::const_iterator i(instances.begin());
                i != instances.end();
                ++i)
        {
            printer.name = i->first;

            //if current instance was not average-based i.e inside loop
            if(i->second.size() == 1)
            {
                printer.looped = false;
                printer.instance_ticks = i->second.back();

                //write current instance's details to console and file
                printer.print();
            }
            else//if profiler was used inside a loop...
            {
                ASSERT_TRUE(i->second.size() >= 2, 
                            "profiling instance has invalid tick count");
                
                printer.looped = true;
                printer.arith_mean_ticks = 0;
                ull_t geo_mean_ticks = 1;
                printer.lowest_ticks = ULLONG_MAX;
                printer.highest_ticks = 0;
                printer.num_iterations = i->second.size();

                for(std::vector<ull_t>::const_iterator j(i->second.begin()); 
                        j != i->second.end();
                        ++j)
                {
                    printer.lowest_ticks = (printer.lowest_ticks > *j) ? *j : printer.lowest_ticks;
                    printer.highest_ticks = (printer.highest_ticks < *j) ? *j : printer.highest_ticks;
                    printer.arith_mean_ticks += *j;
                    geo_mean_ticks *= *j;
                }

                printer.arith_mean_ticks /= i->second.size();
                printer.geo_mean = (double)pow(  geo_mean_ticks, 
                        (1.0 / (double)printer.num_iterations));

                //write current instance's details to console and file
                printer.print();
            }
        } 
    }
    printf(Revert_);

    printf( "\n\nPlease note that an accompanying CVS file containing he above results\n"
            "can also be found in the executable's working directory!\n\n"
            "Thank You!\n\n");
}

void print_kmeans_cmd_help(void)
{
    const char *helpstr = ""
        "CLUSTERING ALGORITHM COMMANDS.\n__\n"
        "Below is a list of the program's kmeans arguments:\n\n"
        "\t--help: display this menu\n\n"
        "\t--pipe_stats <arg>: flag to enable writing profiling stat to a file\n"
        "\tthe possible values for <arg> are either 1 or 2, where 1 is to write\n" 
        "\tresults to a text file and the other to create a csv file.\n\n"
        "\t--ncent <arg>: the number of centroids to spawn for kmeans.\n\n"
        "\t--niter <arg>: the number of kmeans iterations\n\n"
        "\t--seed <arg>: the seed value used in spawning centroids (integral)\n\n"
        "\t--nspe <arg>: the number of SPE invoked to run kmeans.\n\n";

    printf("%s\n", helpstr);
}
