#include <alf_accel.h>
#include <spu_intrinsics.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "../includes/acc_matrix.h"

int kernel(void *task_ctx __attribute__ ((unused)),
           void *param_ctx,
           void *input_buffer,
		   void *inout_buffer __attribute__ ((unused)),
           void *output_buffer,
           unsigned int current_count __attribute__ ((unused)),
           unsigned int total_count __attribute__ ((unused))) 
{
    //unsigned int num = *(unsigned int*)task_ctx;
    ctx_hysteresis_struct ctx = *(ctx_hysteresis_struct*)param_ctx;
    const unsigned short imageWidth = ctx.imageWidth;
	const unsigned short numLines = ctx.numLines;
	const unsigned short highThreshold = ctx.highThreshold;
	const unsigned short lowThreshold = ctx.lowThreshold;
   
    float* mat_one;
	float* mat_two;
    mat_one = (float*) input_buffer;
    mat_two = (float*) output_buffer;
	
	const unsigned short topLeft = 0;
	const unsigned short topMiddle = 1;
	const unsigned short topRight = 2;
	const unsigned short centerLeft = 0 + imageWidth;
	const unsigned short centerMiddle = 1 + imageWidth;
	const unsigned short centerRight = 2 + imageWidth;
	const unsigned short bottomLeft = 0 + imageWidth * 2;
	const unsigned short bottomMiddle = 1 + imageWidth * 2;
	const unsigned short bottomRight = 2 + imageWidth * 2;
	
    unsigned short i,j;
	for(j=0; j < numLines; ++j)
	{
		for(i=0; i<(imageWidth-8); ++i) 
		{
			if (mat_one[j*imageWidth+i+3+centerMiddle] >= highThreshold)
			{
				mat_two[j*imageWidth+i+4] = 255.0f;
			}
			else if (mat_one[j*imageWidth+i+3+centerMiddle] < lowThreshold)
			{
				mat_two[j*imageWidth+i+4] = 0.0f;
			}
			else
			{
				if (mat_one[j*imageWidth+i+3+topLeft] >= highThreshold || 
					mat_one[j*imageWidth+i+3+topMiddle] >= highThreshold || 
					mat_one[j*imageWidth+i+3+topRight] >= highThreshold || 
					mat_one[j*imageWidth+i+3+centerLeft] >= highThreshold || 
					mat_one[j*imageWidth+i+3+centerRight] >= highThreshold || 
					mat_one[j*imageWidth+i+3+bottomLeft] >= highThreshold || 
					mat_one[j*imageWidth+i+3+bottomMiddle] >= highThreshold || 
					mat_one[j*imageWidth+i+3+bottomRight] >= highThreshold )
				{
					mat_two[j*imageWidth+i+4] = 255.0f;
				}
				else
				{
					mat_two[j*imageWidth+i+4] = mat_one[j*imageWidth+i+3+centerMiddle];
				}
			}
		}
	}
	mat_two[j*imageWidth+0] = 0.0f;
	mat_two[j*imageWidth+1] = 0.0f;
	mat_two[j*imageWidth+2] = 0.0f;
	mat_two[j*imageWidth+imageWidth-1] = 0.0f;
	mat_two[j*imageWidth+imageWidth-2] = 0.0f;
	mat_two[j*imageWidth+imageWidth-3] = 0.0f;
    return 0;
}

/* Use macros to export function names */
ALF_ACCEL_EXPORT_API_LIST_BEGIN
   ALF_ACCEL_EXPORT_API("", kernel);
ALF_ACCEL_EXPORT_API_LIST_END
