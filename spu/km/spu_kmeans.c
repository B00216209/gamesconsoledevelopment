#include "spu_kmeans.h"
#include "safe_dma.h"

/*
The code contained within this file perform the following part in the K-Means
Algorithm:

Loop until convergence
    For each data point Xi:
        find the closest centroid Cj -> arg min D(Xi, Cj)
        associate/assign Xi with Cj
    For each cluster J=1 ... K  
        Calculate new position as average of all Xi data points [associated] with Cj
*/

#define MOSTLY_TRUE (1)
#define MOSTLY_FALSE (0)

#define PRINT_DBG_LINES NO

#if PRINT_DBG_LINES
int dbg_counter = 0;
    #define dbg_ printf("\n\nSPU(%d)@line(%d)>>%d<<\n\n", (int)g_spu_idx, __LINE__, ++dbg_counter); \
    fflush(stdout);
#else
    #define dbg_
#endif

entry_data_t e_mem __attribute__((aligned(16)));//no need to align here
centroid_data_t c_mem __attribute__((aligned(16)));
unsigned short attribs[8] __attribute__((aligned(16))); //6 byte pad
ea_block_t ea_block __attribute__((aligned(16)));
atom_t at __attribute__((aligned(128))); 

long g_num_centroids = 0, 
    g_cload = 0,
    g_eload = 0,
    g_num_entries = 0, 
    g_spu_idx = 0, 
    g_num_spes = 0,
    g_max_num_iter = 0; //... allowed for kmeans to run

#define CSPILL ((g_spu_idx == g_num_spes - 1 ) ? g_num_centroids % g_num_spes : 0)
#define ESPILL ((g_spu_idx == g_num_spes - 1 ) ? g_num_entries % g_num_spes : 0)

long dist_func(int a[2], int b[2])
{
    float xd = (float)(a[0] - b[0]);
    float yd = (float)(a[1] - a[1]);
    return (long)sqrtf(pow(xd, 2) + pow(yd, 2));
}

/*
    performs the steps necesary to assign data points 
    to the closest centroids
    also evaluates how many data-points changed centroid-association
    from one iteration to the next.
*/
long evaluate_entry_assignment(void)
{
    //counts how many data-points changes association from one 
    //centroid to another. used to indicate that the algorithm has not 
    //yet converged
    long num_changed_associations = 0;

    long e = 0;
    //for this stage the number of data entry points updated 
    //is equally (as possible) destributed amongst the spus 
    long emax = g_eload + ESPILL;
    //note: values of "e", "emin", and "emax" guarrantee the processing
    //of only a portion of the "entries" array by every spu
    for(e = 0; e < emax; ++e)
    {
        //note here that every spu only processes/updates a portion of
        //entries as expressed via e_mem.ptrs[g_spu_idx][e]
        entry_t *entry = &e_mem.ptrs[g_spu_idx][e];

        long c = 0;    
        long closest_dist = MAX_DIST;
        //"entry" must be check against [every] centroid 
        long idx = 0, elem = 0;
        for(c = 0; c < g_num_centroids; ++c)
        {
            idx = (c < g_cload ? 0 : (c / g_cload));
            if(__builtin_expect(idx >= g_num_spes, MOSTLY_TRUE))
            {
                idx = g_num_spes-1;
            }

            centroid_t *centroid = &c_mem.ptrs[idx][elem];

            long spillover = (idx == g_num_spes -1 ? g_num_centroids % g_num_spes : 0);
            ++elem;
            if(__builtin_expect(elem >= (idx == g_num_spes-1 ? (g_cload + spillover) : g_cload), 
                                MOSTLY_FALSE))
            {
                elem=0;
            }

            long current_dist = dist_func(entry->a, centroid->a);
           
            if (__builtin_expect(   (current_dist < closest_dist), 
                                    MOSTLY_FALSE))
            {
                closest_dist = current_dist;                   
                entry->_u.id = centroid->_u.id;
                //little optimization here to prevent unnecessary iterations
                //after getting as close as is possible
                if (__builtin_expect(current_dist <= 1, MOSTLY_FALSE))
                    break;
            }
            centroid =NULL;
        }

        /*
            this helps us kill two birds with one stonein that if we dont count 
            how many data-points changed affiliation here it means we would have 
            to another for loop else to perform the check.
        */
        if (__builtin_expect(entry->_u.m != entry->_u.id, MOSTLY_FALSE))
        {
            ++num_changed_associations;
            //set previous to current
            entry->_u.m = entry->_u.id;
        }
    }
    return num_changed_associations;
}

/*
    following the previous stage in which data points/ entries
    are associated a centroid this function/ stage calculates the new 
    position for every centroid
*/
void evaluate_centroid_change(void)
{
    long c = 0;
    /*
        **AS ABOVE WITH RESPECT TO PARALLELISM
    */
    long cmax = (g_cload + CSPILL);
    long idx = 0, elem = 0;
    for(c = 0; c < cmax; ++c)
    {
        //note here that every spu only processes/updates a portion of
        //centroids as expressed via c_mem.ptrs[g_spu_idx][c]
        centroid_t *centroid = &c_mem.ptrs[g_spu_idx][c];
        
        coord_t mean_position;
        mean_position._u.x = 0; 
        mean_position._u.y = 0;

        //number of associated data points (reset to zero per iteration)
        centroid->_u.m = 0;

        //loop through all entries and find those which are associated
        //with current centroid
        unsigned short e = 0;

        elem = 0;
        //in this step we [must] loop through all data entries.
        //remember the benefit is in every spu updating only a 
        //portion of centroids (at least in this function)
        for(e = 0; e < g_num_entries; ++e)
        {
            idx = (e < g_eload ? 0 : (e / g_eload));
            if(__builtin_expect(idx >= g_num_spes, MOSTLY_FALSE))
            { 
                idx = g_num_spes - 1;
            }
            entry_t *entry = &e_mem.ptrs[idx][elem];

            int spillover = (idx == g_num_spes -1 ? g_num_entries % g_num_spes : 0);
            ++elem;
            if(__builtin_expect(elem >= (idx == g_num_spes-1 ? (g_eload + spillover) : g_eload), 
                                MOSTLY_FALSE))
            {
                elem=0;
            }

            if (__builtin_expect((centroid->_u.id == entry->_u.id), 0))
            {
                mean_position._u.x += entry->_u.x;
                mean_position._u.y += entry->_u.y;

                //increment number of recognised associates for current centroid
                ++(centroid->_u.m);
            }
        }

        //calculate average distance to centroid from all associated entries
        if (__builtin_expect((centroid->_u.m != 0), 1))
        {
            //calculate new centroid position as average of all associates
            centroid->_u.x = (mean_position._u.x / centroid->_u.m);
            centroid->_u.y = (mean_position._u.y / centroid->_u.m);
        }
    }//for(unsigned short c = cmin; c < cmax; ++c)
}

long spillover = 0;
long sz = 0;
long idx=0;

/*
    Using fixed value tag IDs can result in waits for DMA completions on tag 
    groups containing DMAs issued by other software components.
    Not all 32 tags are available
*/
int mem_read_tag = MFC_TAG_INVALID;
int mem_sync_write_tag = MFC_TAG_INVALID;
int mem_sync_read_tag = MFC_TAG_INVALID;
int mem_write_tag = MFC_TAG_INVALID;

void setup(address_t argp)
{
    dbg_
    /*
        using fixed value tag IDs can result in waits for DMA completions on tag 
        groups containing DMAs issued by other software components. Not all 32 
        tags are available.
    */

    mem_read_tag = mfc_tag_reserve();
    mem_sync_write_tag = mfc_tag_reserve();
    mem_sync_read_tag = mfc_tag_reserve();
    mem_write_tag = mfc_tag_reserve();
    
    dbg_
    /*
     *  the first DMA transaction is to get the effective addresses we need 
     *  for all our data that is on the system RAM
     */
    safe_dma(   FROM_RAM, 
                &ea_block, 
                argp, 
                sizeof(ea_block_t), 
                mem_read_tag, 
                0, 
                0); 
    waitag(mem_read_tag);

    /* here we get the array containing attributes about 
     * our data i.e. array sizes number of spus and thread idx etc.
     * The benefit of this is that we can dynamically allocate memory
     * instead of preallocating static memory arrays based on assumptions
     */
    dbg_
    safe_dma(   FROM_RAM,
                attribs, 
                ea_block.v[EA_ATTRIB], //copy from kmeans data attributes address
                sizeof(unsigned short) * 8,
                mem_read_tag, 
                0, 
                0);
    waitag(mem_read_tag);
    dbg_
    g_num_centroids = (long)attribs[CENTROID_COUNT];
    g_num_entries = (long)attribs[ENTRY_COUNT];
    g_spu_idx = (long)attribs[SPU_IDX];
    g_num_spes = (long)attribs[SPE_COUNT];
    g_max_num_iter = (long)attribs[ITER_COUNT];
    //the portion quantity of centroids and data-points processed by an SPE
    //that is subject to the number of SPEs used in running the algorithm
    g_cload = (g_num_centroids / g_num_spes);
    g_eload = (g_num_entries / g_num_spes);
    dbg_
    /*
     *entry and centroid data pointers are dynamically allocated in local store. 
     *sizes are determined by values previously retrieved using DMA. 
     *Also, we allocate memory that is aligned on a 16 boundary
     *as an optimization for DMA (16 because its is the natural alignment for the
     the type).
     */
    long idx=0;
    for(idx=0; idx < g_num_spes; ++idx)
    {
        spillover = (idx == g_num_spes -1 ? g_num_entries % g_num_spes : 0);
        long sz = sizeof(entry_t) * (g_eload + spillover);
        e_mem.ptrs[idx] = malloc_align( sz, 7);

        safe_dma(   FROM_RAM,
                    e_mem.ptrs[idx], 
                    ea_block.v[EA_EBASE_i + idx], 
                    sz, 
                    mem_read_tag, 
                    0, 
                    0);
    }
    waitag(mem_read_tag);
    dbg_

    //here we simply allocate resources required, DMA is used to get data at a later 
    //stage when it comes to processing data and synchronising
    for(idx=0; idx < g_num_spes; ++idx)
    {
        spillover = (idx == g_num_spes -1 ? g_num_centroids % g_num_spes : 0);
        long sz = sizeof(centroid_t) * (g_cload + spillover);
        c_mem.ptrs[idx] = malloc_align( sz, 7);
    }

    if (__builtin_expect(g_num_spes > 1, MOSTLY_TRUE))
    {
        /*  Retrieve atomic variables to utilize the MFC's atomic unit in 
            performing synchronisation with other system elements
        */
        safe_dma(   FROM_RAM,
                    &at, 
                    ea_block.v[EA_ATOMIC_VAR],  
                    sizeof(atom_t),
                    mem_sync_read_tag, 
                    0, 
                    0);
        waitag(mem_sync_read_tag);
        dbg_
    }
}

long process_data(void)
{
    long iteration __attribute__((aligned(128)));
    long num_changed_assoc __attribute__((aligned(128)));
    bool done = false;
    iteration = 0;

    while(!done)
    {
        /*  
            here we must pull in centroid data every iteration to get update from 
            other SPEs (via main memory). 
            Note: this is only necessary if running using multiple SPEs. If using
            a single SPE we simply pull main memory data [once] and work with the 
            buffered copy in local store (hence the if check below).
        */
        if (__builtin_expect(g_num_spes > 1 || iteration == 0, MOSTLY_TRUE))
        {
            for(idx=0; idx < g_num_spes; ++idx)
            {
                if (__builtin_expect(   idx == g_spu_idx && iteration != 0, 
                                        MOSTLY_FALSE))
                {
                    continue;//pull in my portion only once
                }
                spillover = (idx == g_num_spes -1 ? g_num_centroids % g_num_spes : 0);
                safe_dma(   FROM_RAM,      
                            c_mem.ptrs[idx], 
                            ea_block.v[EA_CBASE_i + idx], 
                            sizeof(centroid_t) * (g_cload + spillover), 
                            mem_read_tag,
                            0,
                            0);
            }
            waitag(mem_read_tag);
            dbg_
        }

        /*
            For each data point Xi:
                1.  find the closest centroid Cj -> arg min D(Xi, Cj)
                2.  associate/assign Xi with Cj

            returns the number of data-points that have changed affiliation
            from one centroid to another.
            Note: in the first iteration all data points will changed affiliation
            from the default value of -1 to an arbitrary centroid id value based
            on proximity.
        */
        num_changed_assoc = evaluate_entry_assignment();
        dbg_
        /*
            The following synchronises data with the PPU and other SPUs (if any)
            after evaluating which Centroids the processed portion of entries are 
            associated with. 

            But first! SPEs must sync memory to share updates 
        */
        if(__builtin_expect((g_num_spes >= 2), MOSTLY_TRUE))
        {
            dbg_
            /*
             *  Transfer updated SPE portion of the Data-Points to main memory
             * */
            safe_dma(   TO_RAM,      
                        e_mem.ptrs[g_spu_idx], 
                        ea_block.v[EA_EBASE_i + g_spu_idx], 
                        sizeof(entry_t) * (g_eload + ESPILL),
                        mem_sync_write_tag, 
                        0, 
                        0);
            waitag(mem_sync_write_tag);
            dbg_  
            {
                //while we are at it update everyone of how may data points changed
                //affiliation (this is in my portion). if local value of "num_changed_assoc"
                //is zero, then no need to modify atomic variable "chng_cnt_addr"
                if (__builtin_expect(num_changed_assoc > 0, MOSTLY_TRUE))
                {
                    dbg_
                    //non zero value globally represents an incomplete state for kmeans
                    atomic_set( (atomic_ea_t)at.chng_cnt_addr, num_changed_assoc);
                    dbg_                    
                }//if (__builtin_expect(num_changed_assoc > 0, 1))

                /*
                 * increment number of customers/ SPUs served. that is those who 
                 * have sent their processed segment to global storage.
                 * */
                atomic_inc((atomic_ea_t)at.served_addr);
            }
            dbg_
            //spinloop until all SPEs notify PPE that they have sent their portion
            //to global storage. The PPE then proceeds in setting "at.served_addr"
            //to 0 signaling for waiting SPEs to continue (hence the condition
            //in the loop below). Refer to Kmeans.cpp in func "run_kmeans()"
            while(atomic_read((atomic_ea_t)at.served_addr) != 0); 

            //when running using multiple SPEs, this value simply indicates whether 
            //one or more SPEs reported that atleast one Data-Point (in their portion) 
            //changed Centroid affiliation.
            //the spinlock above guarrantees that all SPEs will, at this stage,
            //read the same value
            num_changed_assoc = atomic_read((atomic_ea_t)at.chng_cnt_addr);

            if(__builtin_expect(num_changed_assoc ==0, MOSTLY_FALSE))
            {
                done = true;  
                break;//...from kmeans because we have convergence
            }

            /*  
             *  after synchronisation, pull all Data-Points processed by other SPEs
                into my local store to perform Centroid Evaluation stage
             * */
            for(idx=0; idx < g_num_spes; ++idx)
            {
                //no need to read data I already have
                if (__builtin_expect(idx == g_spu_idx, MOSTLY_FALSE))
                    continue;

                spillover = (idx == g_num_spes -1 ? g_num_entries % g_num_spes : 0);
                safe_dma(   FROM_RAM,      
                            e_mem.ptrs[idx], 
                            ea_block.v[EA_EBASE_i + idx], 
                            sizeof(entry_t) * (g_eload + spillover), 
                            mem_sync_read_tag,
                            0,
                            0);
            }
            dbg_
            waitag(mem_sync_read_tag);
            dbg_
        }//if(__builtin_expect((g_num_spes >= 2), 1))

        /*
            Following synchronisation with other SPUs (if any) and the PPU, the
            the program now moves onto evaluating the change in centroid positions.
            Where a given SPU will process a predetermined portion of centroid data.

            The algorithm for this step is as follows:

            For each cluster J=1 ... K  
                1.  Calculate new position as average of all Xi data- 
                    points [associated] with Cj
        */
        dbg_
        evaluate_centroid_change();
        dbg_

        if (__builtin_expect(g_num_spes >= 2, MOSTLY_TRUE))
        {
            /*
             *Now write centroids data back to RAM from SPU's local store
             *note: here we only write back the section of the array
             *the SPU updated, hence c_mem.ptrs[g_spu_idx].

             Note: This (synchronisation) is only necessary if running using
             multiple SPEs. Running with a single SPE means that it need
             only care about the data already in its local store until it 
             finishes. That is, there is nobody else to sync with.
             * */
             safe_dma(  TO_RAM,      
                        c_mem.ptrs[g_spu_idx], 
                        ea_block.v[EA_CBASE_i + g_spu_idx],  
                        sizeof(centroid_t) * (g_cload + CSPILL),
                        mem_write_tag, 
                        0, 
                        0);
            waitag(mem_write_tag);
            {
                dbg_
                //notify and wait
                atomic_inc((atomic_ea_t)at.served_addr);
            }
            dbg_
            while(atomic_read((atomic_ea_t)at.served_addr) != 0); 

            //An SPU gets its centroids updates from others at the top of the loop 
            //i.e. top of "while(!done)"

            dbg_
        }//if (__builtin_expect(g_num_spes >= 2, 1))

        if (__builtin_expect(   !((++iteration) < g_max_num_iter -1) || //reached last iteration
                                num_changed_assoc == 0, //has it converged? (when using one single SPE)
                                0)) //mostly false
        {
            done = true;
        }

        if (__builtin_expect(   g_num_spes >= 2 && g_spu_idx == 0, 
                                MOSTLY_FALSE))
        {
            //notify PPE of current iteration
            atomic_set((atomic_ea_t)at.iter_cnt_addr, iteration);
            //value rest every iteration
            atomic_set((atomic_ea_t)at.chng_cnt_addr, 0);
        }

    }//while(!done)

    return iteration;
}

void teardown(void)
{
    for(idx=0; idx < g_num_spes; ++idx)
    {
        //free centroids' buffer memory
        free_align(c_mem.ptrs[idx]);
        c_mem.ptrs[idx] = NULL;
        dbg_
        //free data-points' buffer memory
        free_align(e_mem.ptrs[idx]);
        e_mem.ptrs[idx] = NULL;
    }

    /*
        release all reserved dma tags
    */
    dbg_
    mfc_tag_release(mem_read_tag);
    mfc_tag_release(mem_sync_write_tag);
    mfc_tag_release(mem_sync_read_tag);
    mfc_tag_release(mem_write_tag);
    dbg_
}

void update_main_memory(long* iterations_consumed)
{
    /*
        If running using a single SPU we now update main memory with local 
        changes of Data-Points and Centroids. 

        This is different for a multi-SPE approach in that updates to main memory 
        are acheived via Atomics-based inter-process memory-synchronisation 
        with other SPEs and the PPE.
    */ 
    if (__builtin_expect((g_num_spes == 1), MOSTLY_FALSE))
    {
        dbg_
        safe_dma(   TO_RAM,      
                    e_mem.ptrs[g_spu_idx], 
                    ea_block.v[EA_EBASE_i + g_spu_idx], 
                    sizeof(entry_t) * g_num_entries, 
                    mem_write_tag,
                    0,
                    0);
        dbg_
        safe_dma(   TO_RAM,      
                    c_mem.ptrs[g_spu_idx], 
                    ea_block.v[EA_CBASE_i + g_spu_idx],  
                    sizeof(centroid_t) * (g_cload + CSPILL),
                    mem_write_tag, 
                    0, 
                    0);
    }
    dbg_
    //the last step involves having the first SPU tell PPE how many iterations 
    //it took before finishing.
    //I know sending a single integer is not the most optimal for DMA but in this 
    //case we are simply reporting back the number of iterations it took before
    //finishing. Could use a mailbox approach but no time :(
    if (__builtin_expect(g_spu_idx == 0, MOSTLY_FALSE))
    {
        safe_dma(   TO_RAM,      
                    iterations_consumed, 
                    ea_block.v[EA_ITER_VAR], 
                    sizeof(int), 
                    mem_write_tag,
                    0,
                    0);
        waitag(mem_write_tag);
    }
    waitag(mem_write_tag);
    dbg_
}

int main(address_t spe_id, address_t argp, address_t envp)
{
    long iterations_consumed[1] __attribute__((aligned(128)));

    setup(argp);
    
    (*iterations_consumed) = process_data();

    update_main_memory(iterations_consumed);

    teardown();

    return 0;
}

