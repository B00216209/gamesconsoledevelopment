#ifndef SPU_KMEANS_H
#define SPU_KMEANS_H

#include <spu_intrinsics.h>
#include <libmisc.h>//malloc_align
#include <malloc.h>//memalign
#include <stdbool.h>
#include <math.h>
#include <libsync.h>

#define MAX_DIST (800) //max distance across the image
typedef unsigned long long ull_t;
typedef ull_t address_t;

// Macro for waiting to completion of DMA group related to input tag:
// 1. Write tag mask
// 2. Read status, blocks until all tag’s DMA complete
#define waitag(t) mfc_write_tag_mask(1 << t); mfc_read_tag_status_all();

#define CENTROID_COUNT (0)
#define ENTRY_COUNT (1)
//spu index (based on thread instance index)
#define SPU_IDX (2) 
#define SPE_COUNT (3)
#define ITER_COUNT (4)

#define EA_EBASE_i (0)
#define EA_CBASE_i (6)
#define EA_ATTRIB (12)
#define EA_ATOMIC_VAR (13)
#define EA_ITER_VAR (14)

typedef struct atom_t
{
    cond_ea_t cashier;
    mutex_ea_t  cashier_mutex, 
                served_mutex;
    address_t   served_addr, //how many spus have update RAM with data-points portion 
                iter_cnt_addr, //iterations consumed
                chng_cnt_addr; //number of data-points that have changed association
                                //from one centroid to another (as reported by every SPU).
}atom_t;

typedef union
{
    struct
    {
        int	x, y,// pixel coordinate
            id,//association value 
            m;//id of the centroid associated with in last iteration
        //for centroid this is the number of associated data points/ entries
    }_u;
    int a[4];
    //vector int v;	
}entity_t, entry_t, centroid_t;

typedef union
{
    struct
    {
        int x, y;	//coords
    }_u;
    int a[2];
}coord_t;

typedef struct
{
    entity_t *ptrs[6];
}entry_data_t, centroid_data_t;

typedef struct
{   
    address_t v[16];  //last elem is padding 
} ea_block_t;

#endif

