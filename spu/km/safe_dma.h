#ifndef SAFE_DMA_H
#define SAFE_DMA_H

#include <stdio.h>
#include <stdlib.h>
#include <spu_mfcio.h>
#include <stdint.h>

#define YES (1)
#define NO (0)

#define DEBUG_MFC NO

#define TO_RAM (0x0)
#define FROM_RAM (0xFF)

/*
 *  performs a dma transfers that exceeds the 16kb limit or less.
 *  the largest size that mfc_get can deal with is 2^14.
 *  For anything larger than this, we need to break it down into chunks
 * */
extern void safe_dma(  	unsigned long direction, 
		                volatile void *ls, 
		                uint64_t ea, 
		                uint32_t size, 
		                uint32_t tag, 
		                uint32_t tid, 
		                uint32_t rid);

#endif
