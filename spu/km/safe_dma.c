/*
    Many thanks to DR. Taylor for the great help on DMA hints and tips
    http://community.dur.ac.uk/j.m.taylor/ps3.html
*/
#include "safe_dma.h"

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))

/*
    A wrapper for mfc_get which checks its parameters and optionally prints out 
    debug information
*/
void custom_mfc_get(volatile void *ls, 
                    uint64_t ea, 
                    uint32_t size, 
                    uint32_t tag, 
                    uint32_t tid, uint32_t rid)
{
#if DEBUG_MFC
    printf("get %p %p %d\n", ls, (void *)ea, size);
#endif
    if ((size > 16) && (((uint32_t)ls & 0xF) || (ea & 0xF) || (ea == 0) || (size > 16384)))
    {
        fprintf(stderr, 
                "custom_mfc_get error (ls=%p, ea = %llx, size = %d)\n", 
                ls, 
                ea, 
                size);
    }
    mfc_get(ls, ea, size, tag, tid, rid);
#if DEBUG_MFC
    /* 
        When debugging we can force the transfer to complete immediately to keep 
        things simple
    */
    mfc_write_tag_mask(0xFF);
    spu_mfcstat(MFC_TAG_UPDATE_ALL);
#endif
}

/*
    A wrapper for mfc_put which checks its parameters and optionally prints out 
    debug information
*/
void custom_mfc_put(volatile void *ls, 
                    uint64_t ea, 
                    uint32_t size, 
                    uint32_t tag, 
                    uint32_t tid, uint32_t rid)
{
#if DEBUG_MFC
    printf("put %p %p %d\n", ls, (void *)ea, size);
#endif
    if (((uint32_t)ls & 0xF) || (ea & 0xF) || (ea == 0) || (size > 16384))
    {
        fprintf(stderr, 
                "custom_mfc_put error (ls=%p, ea = %llx, size = %d)\n", 
                ls, 
                ea, 
                size);
    }
    mfc_put(ls, ea, size, tag, tid, rid);
#if DEBUG_MFC
    /* 
        When debugging we can force the transfer to complete immediately to keep 
        things simple
    */
    mfc_write_tag_mask(0xFF);
    spu_mfcstat(MFC_TAG_UPDATE_ALL);
#endif
}

/*
 *  performs a dma transfers that exceeds the 16kb limit and those less
 *  the largest size that mfc_get can deal with is 2^14.
 *  For anything larger than this, we need to break it down into chunks
 * */
void safe_dma(  unsigned long direction, 
                volatile void *ls, 
                uint64_t ea, 
                uint32_t size, 
                uint32_t tag, 
                uint32_t tid, 
                uint32_t rid)
{
    while (size)
    {
        uint32_t thisSize = MIN(size, 16384);

        switch(direction) 
        {
            case FROM_RAM:
            custom_mfc_get(ls, ea, thisSize, tag, tid, rid);
            break;
            case TO_RAM:
            custom_mfc_put(ls, ea, thisSize, tag, tid, rid);
            break;
        };

        size -= thisSize;
        ls = (void *)((char *)ls + thisSize);
        ea += thisSize;
    }
}
