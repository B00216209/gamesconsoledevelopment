#include <alf_accel.h>
#include <spu_intrinsics.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "../includes/acc_matrix.h"

int kernel(void *task_ctx __attribute__ ((unused)),
           void *param_ctx,
           void *input_buffer,
		   void *inout_buffer __attribute__ ((unused)),
           void *output_buffer,
           unsigned int current_count __attribute__ ((unused)),
           unsigned int total_count __attribute__ ((unused))) 
{
	//unsigned int num = *(unsigned int*)task_ctx;
    ctx_sobel_struct ctx = *(ctx_sobel_struct*)param_ctx;
    const unsigned short imageWidth = ctx.imageWidth;
	const unsigned short numLines = ctx.numLines;
   
    float* mat_one;
	float* mat_two;
    mat_one = (float*) input_buffer;
    mat_two = (float*) output_buffer;
	
	const unsigned short topLeft = 0;
	const unsigned short topMiddle = 1;
	const unsigned short topRight = 2;
	const unsigned short centerLeft = 0 + imageWidth;
	//const unsigned int centerMiddle = 1 + imageWidth;
	const unsigned short centerRight = 2 + imageWidth;
	const unsigned short bottomLeft = 0 + imageWidth * 2;
	const unsigned short bottomMiddle = 1 + imageWidth * 2;
	const unsigned short bottomRight = 2 + imageWidth * 2;
	
    unsigned short i,j;
	float xMask, yMask;
	float direction;
	for(j=0; j < numLines; ++j)
	{
		for(i=0; i<(imageWidth-8); ++i) 
		{
			xMask = 	-mat_one[j*imageWidth+i+3+topLeft] 			+ 													mat_one[j*imageWidth+i+3+topRight]			+ 
						mat_one[j*imageWidth+i+3+centerLeft]*-2 	+ 													mat_one[j*imageWidth+i+3+centerRight]*2 	+ 
						-mat_one[j*imageWidth+i+3+bottomLeft]		+  									 				mat_one[j*imageWidth+i+3+bottomRight];	
						
			yMask = 	-mat_one[j*imageWidth+i+3+topLeft] 			+ 	mat_one[j*imageWidth+i+3+topMiddle]*-2 		+ 	-mat_one[j*imageWidth+i+3+topRight]			+ 
						
						mat_one[j*imageWidth+i+3+bottomLeft] 		+ 	mat_one[j*imageWidth+i+3+bottomMiddle]*2 	+ 	mat_one[j*imageWidth+i+3+bottomRight];			
			
			mat_two[j*imageWidth+i+4] = sqrtf(xMask * xMask + yMask * yMask);
			// Following code is an altered code section from the calc_gradient_sobel function found at https://code.google.com/p/fast-edge/source/browse/trunk/%20fast-edge/fast-edge.c
			if(xMask == 0.0f)
			{
				mat_two[j*imageWidth+i+4+imageWidth*numLines] = 2.0f;
			}
			else
			{
				direction = yMask / xMask;
				if (direction < 0)
				{
					if (direction < -2.41421356237f)
					{
						mat_two[j*imageWidth+i+4+imageWidth*numLines] = 0.0f;
					} 
					else 
					{
						if (direction < -0.414213562373f)
						{
							mat_two[j*imageWidth+i+4+imageWidth*numLines] = 1.0f;
						} 
						else 
						{
							mat_two[j*imageWidth+i+4+imageWidth*numLines] = 2.0f;
						}
					}
				} 
				else 
				{
					if (direction > 2.41421356237f)
					{
						mat_two[j*imageWidth+i+4+imageWidth*numLines] = 0.0f;
					} 
					else 
					{
						if (direction > 0.414213562373f)
						{
							mat_two[j*imageWidth+i+4+imageWidth*numLines] = 3.0f;
						} 
						else 
						{
							mat_two[j*imageWidth+i+4+imageWidth*numLines] = 2.f;
						}
					}
				}
			}
		}
	
		mat_two[j*imageWidth+0] = 0.0f;
		mat_two[j*imageWidth+1] = 0.0f;
		mat_two[j*imageWidth+2] = 0.0f;
		mat_two[j*imageWidth+3] = 0.0f;
		mat_two[j*imageWidth+imageWidth-4] = 0.0f;
		mat_two[j*imageWidth+imageWidth-3] = 0.0f;
		mat_two[j*imageWidth+imageWidth-2] = 0.0f;
		mat_two[j*imageWidth+imageWidth-1] = 0.0f;
		
		mat_two[j*imageWidth+0+imageWidth*numLines] = 0.0f;
		mat_two[j*imageWidth+1+imageWidth*numLines] = 0.0f;
		mat_two[j*imageWidth+2+imageWidth*numLines] = 0.0f;
		mat_two[j*imageWidth+3+imageWidth*numLines] = 0.0f;
		mat_two[j*imageWidth+imageWidth-4+imageWidth*numLines] = 0.0f;
		mat_two[j*imageWidth+imageWidth-3+imageWidth*numLines] = 0.0f;
		mat_two[j*imageWidth+imageWidth-2+imageWidth*numLines] = 0.0f;
		mat_two[j*imageWidth+imageWidth-1+imageWidth*numLines] = 0.0f;	
	}
    return 0;
}

/* Use macros to export function names */
ALF_ACCEL_EXPORT_API_LIST_BEGIN
   ALF_ACCEL_EXPORT_API("", kernel);
ALF_ACCEL_EXPORT_API_LIST_END
