#include <alf_accel.h>
#include <spu_intrinsics.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "../includes/acc_matrix.h"

int kernel(void *task_ctx __attribute__ ((unused)),
           void *param_ctx,
           void *input_buffer,
		   void *inout_buffer __attribute__ ((unused)),
           void *output_buffer,
           unsigned int current_count __attribute__ ((unused)),
           unsigned int total_count __attribute__ ((unused))) 
{
    //unsigned int num = *(unsigned int*)task_ctx;
    ctx_NMS_struct ctx = *(ctx_NMS_struct*)param_ctx;
    const unsigned short imageWidth = ctx.imageWidth;
	const unsigned short numLines = ctx.numLines;
   
    float* mat_one;
	float* mat_two;
    mat_one = (float*) input_buffer;
    mat_two = (float*) output_buffer;
	
	const unsigned short topLeft = 0;
	const unsigned short topMiddle = 1;
	const unsigned short topRight = 2;
	const unsigned short centerLeft = 0 + imageWidth;
	const unsigned short centerMiddle = 1 + imageWidth;
	const unsigned short centerRight = 2 + imageWidth;
	const unsigned short bottomLeft = 0 + imageWidth * 2;
	const unsigned short bottomMiddle = 1 + imageWidth * 2;
	const unsigned short bottomRight = 2 + imageWidth * 2;
	const unsigned short direction = 1 + imageWidth * (2 + numLines);
	
    unsigned short i,j;
	for(j=0; j < numLines; ++j)
	{
		for(i=0; i<(imageWidth-8); ++i) 
		{
			switch((int)mat_one[j*imageWidth+i+3+direction])
			{
				case 0: 
					// Vertical Edge
					// If pixel is more intense than it's edge neighbours
					if (mat_one[j*imageWidth+i+3+centerMiddle] > mat_one[j*imageWidth+i+3+topMiddle] && mat_one[j*imageWidth+i+3+centerMiddle] > mat_one[j*imageWidth+i+3+bottomMiddle])
					{
						// Clamp at 255 to avoid overflows on 8 bit png image save
						if(mat_one[j*imageWidth+i+3+centerMiddle] > 255)
						{
							mat_two[j*imageWidth+i+4] = 255;
						}
						else
						{
							mat_two[j*imageWidth+i+4] = mat_one[j*imageWidth+i];
						}
					}
					else
					{
						mat_two[j*imageWidth+i+4] = 0;
					}
					break;
				case 1: 
					// 45 degree incline
					// If pixel is more intense than it's edge neighbours
					if (mat_one[j*imageWidth+i+3+centerMiddle] > mat_one[j*imageWidth+i+3+topRight] && mat_one[j*imageWidth+i+3+centerMiddle] > mat_one[j*imageWidth+i+3+bottomLeft])
					{
						// Clamp at 255 to avoid overflows on 8 bit png image save
						if(mat_one[j*imageWidth+i+3+centerMiddle] > 255)
						{
							mat_two[j*imageWidth+i+4] = 255;
						}
						else
						{
							mat_two[j*imageWidth+i+4] = mat_one[j*imageWidth+i+3+centerMiddle];
						}
					}
					else
					{
						mat_two[j*imageWidth+i+4] = 0;
					}
					break;
				case 2: 
					// Horizontal Edge
					// If pixel is more intense than it's edge neighbours
					if (mat_one[j*imageWidth+i+3+centerMiddle] > mat_one[j*imageWidth+i+3+centerLeft] && mat_one[j*imageWidth+i+3+centerMiddle] > mat_one[j*imageWidth+i+3+centerRight])
					{
						// Clamp at 255 to avoid overflows on 8 bit png image save
						if(mat_one[j*imageWidth+i+3+centerMiddle] > 255)
						{
							mat_two[j*imageWidth+i+4] = 255;
						}
						else
						{
							mat_two[j*imageWidth+i+4] = mat_one[j*imageWidth+i+3+centerMiddle];
						}
					}
					else
					{
						mat_two[j*imageWidth+i+4] = 0;
					}
					break;
				case 3: 
					// 45 degree decline
					// If pixel is more intense than it's edge neighbours
					if (mat_one[j*imageWidth+i+3+centerMiddle] > mat_one[j*imageWidth+i+3+topLeft] && mat_one[j*imageWidth+i+3+centerMiddle] > mat_one[j*imageWidth+i+3+bottomRight])
					{
						// Clamp at 255 to avoid overflows on 8 bit png image save
						if(mat_one[j*imageWidth+i+3+centerMiddle] > 255)
						{
							mat_two[j*imageWidth+i+4] = 255;
						}
						else
						{
							mat_two[j*imageWidth+i+4] = mat_one[j*imageWidth+i+3+centerMiddle];
						}
					}
					else
					{
						mat_two[j*imageWidth+i+4] = 0;
					}
					break;
				default: 
						mat_two[j*imageWidth+i+4] = 128; // for testing case failutes
					break;
			}
		}
	}
	mat_two[j*imageWidth+0] = 0;
	mat_two[j*imageWidth+1] = 0;
	mat_two[j*imageWidth+2] = 0;
	mat_two[j*imageWidth+3] = 0;
	mat_two[j*imageWidth+imageWidth-4] = 0;
	mat_two[j*imageWidth+imageWidth-3] = 0;
	mat_two[j*imageWidth+imageWidth-2] = 0;
	mat_two[j*imageWidth+imageWidth-1] = 0;
    return 0;
}

/* Use macros to export function names */
ALF_ACCEL_EXPORT_API_LIST_BEGIN
   ALF_ACCEL_EXPORT_API("", kernel);
ALF_ACCEL_EXPORT_API_LIST_END
