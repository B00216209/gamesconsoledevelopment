#include <alf_accel.h>
#include <spu_intrinsics.h>
#include <stdio.h>
#include <string.h>
#include "../includes/acc_matrix.h"

  

int kernel(void *task_ctx __attribute__ ((unused)),
           void *param_ctx,
           void *input_buffer,
		   void *inout_buffer __attribute__ ((unused)),
           void *output_buffer,
           unsigned int current_count __attribute__ ((unused)),
           unsigned int total_count __attribute__ ((unused))) 
{
    ctx_blur_struct ctx = *(ctx_blur_struct*)param_ctx;
    const unsigned short imageWidth = ctx.imageWidth;
	const unsigned short numLines = ctx.numLines;
   
	float* mat_one = (float*) input_buffer;
	float* mat_two = (float*) output_buffer;
 	
	const unsigned short k00 = 0;
	const unsigned short k01 = 1;
	const unsigned short k02 = 2;
	const unsigned short k03 = 3;
	const unsigned short k04 = 4;
	const unsigned short k05 = 5;
	const unsigned short k06 = 6;
	const unsigned short k10 = 0 + imageWidth;
	const unsigned short k11 = 1 + imageWidth;
	const unsigned short k12 = 2 + imageWidth;
	const unsigned short k13 = 3 + imageWidth;
	const unsigned short k14 = 4 + imageWidth;
	const unsigned short k15 = 5 + imageWidth;
	const unsigned short k16 = 6 + imageWidth;
	const unsigned short k20 = 0 + imageWidth*2;
	const unsigned short k21 = 1 + imageWidth*2;
	const unsigned short k22 = 2 + imageWidth*2;
	const unsigned short k23 = 3 + imageWidth*2;
	const unsigned short k24 = 4 + imageWidth*2;
	const unsigned short k25 = 5 + imageWidth*2;
	const unsigned short k26 = 6 + imageWidth*2;
	const unsigned short k30 = 0 + imageWidth*3;
	const unsigned short k31 = 1 + imageWidth*3;
	const unsigned short k32 = 2 + imageWidth*3;
	const unsigned short k33 = 3 + imageWidth*3;
	const unsigned short k34 = 4 + imageWidth*3;
	const unsigned short k35 = 5 + imageWidth*3;
	const unsigned short k36 = 6 + imageWidth*3;
	const unsigned short k40 = 0 + imageWidth*4;
	const unsigned short k41 = 1 + imageWidth*4;
	const unsigned short k42 = 2 + imageWidth*4;
	const unsigned short k43 = 3 + imageWidth*4;
	const unsigned short k44 = 4 + imageWidth*4;
	const unsigned short k45 = 5 + imageWidth*4;
	const unsigned short k46 = 6 + imageWidth*4;
	const unsigned short k50 = 0 + imageWidth*5;
	const unsigned short k51 = 1 + imageWidth*5;
	const unsigned short k52 = 2 + imageWidth*5;
	const unsigned short k53 = 3 + imageWidth*5;
	const unsigned short k54 = 4 + imageWidth*5;
	const unsigned short k55 = 5 + imageWidth*5;
	const unsigned short k56 = 6 + imageWidth*5;
	const unsigned short k60 = 0 + imageWidth*6;
	const unsigned short k61 = 1 + imageWidth*6;
	const unsigned short k62 = 2 + imageWidth*6;
	const unsigned short k63 = 3 + imageWidth*6;
	const unsigned short k64 = 4 + imageWidth*6;
	const unsigned short k65 = 5 + imageWidth*6;
	const unsigned short k66 = 6 + imageWidth*6;
	const float f0 = 0.00001965191612403453f;
	const float f1 = 0.00023940934949729186f;
	const float f2 = 0.0010729582649787318f;
	const float f3 = 0.0017690091140439247f;
	const float f4 = 0.002916602954386583f;
	const float f5 = 0.013071307583189733f;
	const float f6 = 0.021550942848268615f;
	const float f7 = 0.058581536330607024f;
	const float f8 = 0.09658462501856331f;
	const float f9 = 0.1592411256906998f;
	
	unsigned int i,j;
	
	for(j=0; j < numLines; ++j)
	{
		for(i=0; i<(imageWidth-6); ++i) 
		{
		/*	mat_two[j*imageWidth+i+2] = (	mat_one[j*imageWidth+i+k00]*2	+ mat_one[j*imageWidth+i+k01]*4 + 	mat_one[j*imageWidth+i+k02]*5 	+ mat_one[j*imageWidth+i+k03]*4 	+ mat_one[j*imageWidth+i+k04]*2 + 
											mat_one[j*imageWidth+i+k10]*4	+ mat_one[j*imageWidth+i+k11]*9 + 	mat_one[j*imageWidth+i+k12]*12 	+ mat_one[j*imageWidth+i+k13]*9 	+ mat_one[j*imageWidth+i+k14]*4 +  
											mat_one[j*imageWidth+i+k20]*5	+ mat_one[j*imageWidth+i+k21]*12+ 	mat_one[j*imageWidth+i+k22]*15 	+ mat_one[j*imageWidth+i+k23]*12 	+ mat_one[j*imageWidth+i+k24]*5 + 
											mat_one[j*imageWidth+i+k30]*4	+ mat_one[j*imageWidth+i+k31]*9 + 	mat_one[j*imageWidth+i+k32]*12 	+ mat_one[j*imageWidth+i+k33]*9 	+ mat_one[j*imageWidth+i+k34]*4 + 
											mat_one[j*imageWidth+i+k40]*2	+ mat_one[j*imageWidth+i+k41]*4 + 	mat_one[j*imageWidth+i+k42]*5 	+ mat_one[j*imageWidth+i+k43]*4 	+ mat_one[j*imageWidth+i+k44]*2	)/159;*/
			mat_two[j*imageWidth+i+3] = 	mat_one[j*imageWidth+i+k00]*f0 + mat_one[j*imageWidth+i+k01]*f1 + mat_one[j*imageWidth+i+k02]*f2 + mat_one[j*imageWidth+i+k03]*f3 + mat_one[j*imageWidth+i+k04]*f2 + mat_one[j*imageWidth+i+k05]*f1 + mat_one[j*imageWidth+i+k06]*f0 +
											mat_one[j*imageWidth+i+k10]*f1 + mat_one[j*imageWidth+i+k11]*f4 + mat_one[j*imageWidth+i+k12]*f5 + mat_one[j*imageWidth+i+k13]*f6 + mat_one[j*imageWidth+i+k14]*f5 + mat_one[j*imageWidth+i+k15]*f4 + mat_one[j*imageWidth+i+k16]*f1 +
											mat_one[j*imageWidth+i+k20]*f2 + mat_one[j*imageWidth+i+k21]*f5 + mat_one[j*imageWidth+i+k22]*f7 + mat_one[j*imageWidth+i+k23]*f8 + mat_one[j*imageWidth+i+k24]*f7 + mat_one[j*imageWidth+i+k25]*f5 + mat_one[j*imageWidth+i+k26]*f2 +
											mat_one[j*imageWidth+i+k30]*f3 + mat_one[j*imageWidth+i+k31]*f6 + mat_one[j*imageWidth+i+k32]*f8 + mat_one[j*imageWidth+i+k33]*f9 + mat_one[j*imageWidth+i+k34]*f8 + mat_one[j*imageWidth+i+k35]*f6 + mat_one[j*imageWidth+i+k36]*f3 +
											mat_one[j*imageWidth+i+k40]*f2 + mat_one[j*imageWidth+i+k41]*f5 + mat_one[j*imageWidth+i+k42]*f7 + mat_one[j*imageWidth+i+k43]*f8 + mat_one[j*imageWidth+i+k44]*f7 + mat_one[j*imageWidth+i+k45]*f5 + mat_one[j*imageWidth+i+k46]*f2 +
											mat_one[j*imageWidth+i+k50]*f1 + mat_one[j*imageWidth+i+k51]*f4 + mat_one[j*imageWidth+i+k52]*f5 + mat_one[j*imageWidth+i+k53]*f6 + mat_one[j*imageWidth+i+k54]*f5 + mat_one[j*imageWidth+i+k55]*f4 + mat_one[j*imageWidth+i+k56]*f1 +
											mat_one[j*imageWidth+i+k60]*f0 + mat_one[j*imageWidth+i+k61]*f1 + mat_one[j*imageWidth+i+k62]*f2 + mat_one[j*imageWidth+i+k63]*f3 + mat_one[j*imageWidth+i+k64]*f2 + mat_one[j*imageWidth+i+k65]*f1 + mat_one[j*imageWidth+i+k66]*f0;
		}
		mat_two[j*imageWidth+0] = 0.0f;
		mat_two[j*imageWidth+1] = 0.0f;
		mat_two[j*imageWidth+2] = 0.0f;
		mat_two[j*imageWidth+imageWidth-3]  = 0.0f;
		mat_two[j*imageWidth+imageWidth-2]  = 0.0f;
		mat_two[j*imageWidth+imageWidth-1]  = 0.0f;
	}
    return 0;
}

/* Use macros to export function names */
ALF_ACCEL_EXPORT_API_LIST_BEGIN
   ALF_ACCEL_EXPORT_API("", kernel);
ALF_ACCEL_EXPORT_API_LIST_END
