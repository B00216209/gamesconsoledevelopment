PCC=ppu-g++
SCC=spu-gcc
PCCFLAGS= -Wall -O2 -ftree-vectorize -mcpu=cell -maltivec -mabi=altivec -finline-functions
SCCFLAGS= -Wall -O2 -ftree-vectorize -finline-functions
PPU_LIBS=-L$(CELL_TOP)/src/lib/sync/ppu64 -lalf -lspe2 -ldl -lpthread -lsync
SPU_LIBS=-L/usr/spu/lib -L$(CELL_TOP)/usr/spu/lib -L$(CELL_TOP)/src/lib/misc/spu -lalf -lmisc -lsync
EMBED=ppu-embedspu
PROC_TYPE=-m64
PPU_LD=ppu-ld
SHARED=-shared

all: cw spu_programs.so

cw: main.cpp kmeans.cpp argvparser.cpp common.cpp lodepng.cpp spu_kmeans.o 
	$(PCC) $(PPU_INCLUDES) -o $@ $^ $(PCCFLAGS) $(PPU_LIBS) -I$(CELL_TOP)/usr/include
   
spu_programs.so: spu_sobel.o spu_blur.o spu_NMS.o spu_hysteresis.o
	$(PPU_LD) $(SHARED) -o $@ $^

spu_sobel.o: spu_sobel
	$(EMBED) $(PROC_TYPE) $^ $^ $@
     
spu_sobel: ./spu/spu_sobel.c
	$(SCC) $(SPU_INCLUDES) -o $@ $^  $(SCCFLAGS) $(SPU_LIBS)
	
spu_blur.o: spu_blur
	$(EMBED) $(PROC_TYPE) $^ $^ $@
     
spu_blur: ./spu/spu_blur.c
	$(SCC) $(SPU_INCLUDES) -o $@ $^  $(SCCFLAGS) $(SPU_LIBS)
	
spu_NMS.o: spu_NMS
	$(EMBED) $(PROC_TYPE) $^ $^ $@
     
spu_NMS: ./spu/spu_NMS.c
	$(SCC) $(SPU_INCLUDES) -o $@ $^  $(SCCFLAGS) $(SPU_LIBS)
	
spu_hysteresis.o: spu_hysteresis
	$(EMBED) $(PROC_TYPE) $^ $^ $@
     
spu_hysteresis: ./spu/spu_hysteresis.c
	$(SCC) $(SPU_INCLUDES) -o $@ $^  $(SCCFLAGS) $(SPU_LIBS)

spu_kmeans.o: spu_kmeans
	$(EMBED) $(PROC_TYPE) $^ $^ $@
     
spu_kmeans: ./spu/km/spu_kmeans.c ./spu/km/spu_kmeans.h ./spu/km/safe_dma.c ./spu/km/safe_dma.h
	$(SCC) $(SPU_INCLUDES) -o $@ $^ $(SCCFLAGS) $(SPU_LIBS) -lm -I$(CELL_TOP)/usr/include
	

.PHONY: clean

.SECONDARY: %.o

clean: 
	rm -f *.o spu_sobel spu_blur spu_kmeans spu_NMS spu_hysteresis cw spu_programs.so
	
include	$(CELL_TOP)/buildutils/make.footer
   
