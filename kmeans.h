#ifndef __KMEANS_H__
#define __KMEANS_H__

#include "common.h"
#include  <libsync.h>

struct atom_t
{
    cond_ea_t cashier;
    mutex_ea_t cashier_mutex, served_mutex;
    address_t   served_addr, 
                iter_cnt_addr, 
                chng_cnt_addr;
};

union entity_t
{
    struct
    {
        int	x, y,// pixel coordinate
            id,//association value 
            m; //id from prev iter/ num associated entries
    }_u;
    int a[4];
    vector int v;	
};

typedef entity_t entry_t;
typedef entity_t centroid_t;

union coord_t
{
    struct
    {
        int x, y;	//coords
    }_u;
    int a[2];
};

struct ea_block_t
{
    address_t v[16];
};

typedef struct
{
    entity_t *ptrs[6];
}entry_data_t, centroid_data_t;

/*
   holds values specific to every spu
note: by insuring members taking up the 
largest storage are declared first, we insure
a tightly packed struct of the smallest possible
size.
*/
struct control_block_t
{
    ea_block_t ea_block __attribute__((aligned(16)));

    /*
    	0: num_centroids 
    	1: num_entries 
    	2: spu_index
    	3: num_spes
        4: num iterations
    */
    unsigned short attribs[8] __attribute__((aligned(16))); 
};

/*
	holds data used to envoke a pthread (void* arg)
*/
struct ppu_thread_data_t
{
    control_block_t cb;
    spe_context_ptr_t spe_id;
};

#endif
