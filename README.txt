# COMMAND LINE PARAMETERS 
	none: program uses default filepath of AT3_1m4_01 and runs for 1 count on 6 spus
	--help || -h: Prints command line help info.
	--filename <value>: pName of input file minus it's index. Default value: AT3_1m4_
	--filenameCount <value>: Number of filename indexes to run. Default value: 1
	--spuCount <value>: Name of spu's to utilise. Default value: 6
	--blockSize <value>: Name of lines to output per spu workblock. Default value: 1
	--noOutput: Disables program PNG ouput, useful when used with --benchmark to speed up iterations.
	--benchmark <value>: Number of iterations to run entire program on input images, used to profile performance. Default value 1


# If adding a solo .h file , put it in the /includes directory

# If adding a PPU based .cpp file, put both it and it's partner .h file in the root directory then make the following changes to the makefile:
	# Alter this line
	ppu_$(PROJ): ppu_$(PROJ).cpp lodepng.cpp common.cpp <FILENAME.cpp>

# If adding a SPU based .c file, put it in the /spu directory then make the following changes to the makefile:
	# Alter this line
	spu_programs.so: spu_sobel.o spu_blur.o <FILENAME>.o
		$(PCC) $(PPU_INCLUDES) -o $@ $^ $(PCCFLAGS) $(PPU_LIBS) -I$(CELL_TOP)/usr/include
	
	# Make a copy of this block of lines below the last isntance of it but above the line: .PHONY: clean 
	<FILENAME.o>: <FILENAME>
		$(EMBED) $(PROC_TYPE) $^ $^ $@
		 
	<FILENAME>: ./spu/<FILENAME.c>
		$(SCC) $(SPU_INCLUDES) -o $@ $^  $(SCCFLAGS) $(SPU_LIBS)
		
	# Alter this line
	clean: rm -f *.o spu_sobel spu_blur <filename>
	
# If you require any additional libraries edit the appropriate variable in the makefile based on the type of file you need it for:
	# Alter these lines
	PPU_LIBS=-lalf -lspe2 -ldl -l<libraryname>
	SPU_LIBS=-L/usr/spu/lib -lalf -l<libraryname>
	
# To change compiler flags for either SPU or PPU code alter the relevant linse below in the makefile
	# Alter these lines
	PCCFLAGS= -Wall -O2
	SCCFLAGS= -Wall -O0